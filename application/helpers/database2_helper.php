<?php
if (!function_exists('database2')) {
    function database2($dbname, $username, $password, $servername)
    {
        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $dbname);

        // Check connection
       
        return $conn;
    }
}
