<?php
defined('BASEPATH') or exit('No direct script access allowed');

$config = array(
    'protocol' => 'smtp',
    'smtp_host' => 'tls://smtp.gmail.com',
    'smtp_port' => 587,
    'smtp_user' => '', // email pengirim
    'mailtype' => 'html',
    'charset' => 'iso-8859-1',
    'SMTPAuth' => true
);
