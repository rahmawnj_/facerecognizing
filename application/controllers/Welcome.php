<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Welcome extends CI_Controller
{
	
	public function index($id = null)
	{
		$this->load->view('welcome_message', [
			'id' => $id
		]);
	}
	
	public function tes(){
		$this->load->view('tes', [
		]);
	}
}
