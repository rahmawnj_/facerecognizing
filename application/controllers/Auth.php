<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model(['admin_model']);
	}

	public function index()
	{
		if ($this->session->userdata('status')) {
			redirect('dashboard');
		}
		$data['title'] = 'Login';
		$this->load->view('auth/login', $data);
	}


	public function login()
	{
		if ($this->session->userdata('status')) {
			redirect('dashboard');
		}
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		if ($this->form_validation->run() == false) {
			$this->index();
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$user =  $this->admin_model->get_admin('username', $username)[0];

			if ($user) {
				if (password_verify($password, $user['password'])) {
					$this->session->set_userdata([
						'id_admin_face' => $user['id_user'],
						'username' => $user['username'],
						'name' => $user['name'],
						'role' => $user['role'],
						'photo' => $user['photo'],
						'status' => true
					]);
					redirect('dashboard');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
						<div class="alert-message">
						Password Salah!
						</div>
					</div>');
					redirect('auth/login');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
				<div class="alert-message">
				Username tidak terdaftar!
				</div>
			</div>');
				redirect('auth/login');
			}
		}
	}

	public function logout()
	{

		$this->session->unset_userdata(['username', 'name', 'photo', 'status', 'gambar']);
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
				Berhasil Logout!
			  </div>');
		redirect('/');
	}
}
