<?php

use function PHPSTORM_META\type;

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model(['memberimage_model', 'member_model', 'log_model', 'device_model', 'admin_model']);
		$this->load->helper(['form', 'url',]);
		if (!$this->session->userdata('status')) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
			redirect('auth/login');
		}
	}

	public function member_search_update()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		// $device = $this->device_model->get_device('master', 1);
		$device = $this->db->get_where('devicefacerecognition', ['deviceid' => $this->input->post('deviceid')])->row_array();


		$id_member = $this->input->post('id_member');

		// $member = $this->db->get_where('gymmember', ['id' => $id_member])->row_array();
		$member = $this->db->get_where('members', ['id_member' => $id_member])->row_array();

		$path_gambar = FCPATH . 'assets/img/uploads/' . $member['photo'];
		$gambar = file_get_contents($path_gambar);
		$gambar_format = 'data:' . get_mime_by_extension($member['photo']) . ';base64,' . base64_encode($gambar);


		$data = array(
			"operator" => "EditPerson",
			"info" => array(
				"DeviceID" => $device['deviceid'],
				"IdType" => 0,
				"CustomizeID" => $this->input->post('uuid'),
				"PersonUUID" => $this->input->post('uuid'),
			),
			"picinfo" => $gambar_format
		);
		$data_string = json_encode($data, JSON_UNESCAPED_SLASHES);

		// URL tujuan API
		$url =  $device['ip_address'] . '/action/EditPerson';

		// Inisialisasi cURL
		$ch = curl_init($url);

		// Set auth basic
		curl_setopt($ch, CURLOPT_USERPWD, "admin:admin");

		// Set method POST dan tambahkan data yang akan dikirimkan
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		// Eksekusi cURL
		$result = json_decode(curl_exec($ch), true);
		// Tutup cURL

		curl_close($ch);
		if ($result['code'] == 200) {
			$this->session->set_flashdata('success', 'Foto Member Berhasil Diubah!');
		} else {
			$this->session->set_flashdata('error', 'Foto Member Gagal Diubah!');
		}
		redirect('/dashboard/member_search/id_device=17&id_member=26');
	}

	public function member_search_delete()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$customizeID = $this->input->get('CustomizeID');
		$deviceID = $this->input->get('DeviceID');

		$device = $this->db->get_where('devicefacerecognition', ['deviceid' => $deviceID])->row_array();
		$data_string = json_encode([
			"operator" => "DeletePerson",
			"info" => array(
				"DeviceID" => $deviceID,
				"TotalNum" => 1,
				"CustomizeID" => [(int)$customizeID],
				"PersonUUID" => [$customizeID],
			)
		], JSON_UNESCAPED_SLASHES);
		// URL tujuan API
		$url =  $device['ip_address'] . '/action/DeletePerson';
		// Inisialisasi cURL
		$ch = curl_init($url);
		// Set auth basic
		curl_setopt($ch, CURLOPT_USERPWD, "admin:admin");
		// Set method POST dan tambahkan data yang akan dikirimkan
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		// Eksekusi cURL
		$result = json_decode(curl_exec($ch), true);

		// Tutup cURL
		curl_close($ch);
		if ($result['code'] == 200) {
			$this->session->set_flashdata('success', 'Foto Member Berhasil Dihapus!');
		} else {
			$this->session->set_flashdata('error', 'Foto Member Gagal Dihapus! ' . $result['info']['Detail']);
		}
		redirect('/dashboard/member_search');
	}
	public function member_search()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$id_device = $this->input->get('id_device');
		$id_member = $this->input->get('id_member');

		$device = $this->db->get_where('devicefacerecognition', ['id' => $id_device])->row_array();
		$member = $this->db->get_where('members', ['id_member' => $id_member])->row_array();
		$memberImage = [];
		if ($member) {
			$data_string = json_encode([
				"operator" => "SearchPersonList",
				"info" => array(
					"DeviceID" => $device['deviceid'],
					"Picture" => 1,
					"Name" => $member['nama'],
					"CustomizeID" => $member['id_member'],
					"PersonUUID" => $member['id_member'],
				),
			], JSON_UNESCAPED_SLASHES);
			// URL tujuan API
			$url =  $device['ip_address'] . '/action/SearchPersonList';
			// Inisialisasi cURL
			$ch = curl_init($url);
			// Set auth basic
			curl_setopt($ch, CURLOPT_USERPWD, "admin:admin");
			// Set method POST dan tambahkan data yang akan dikirimkan
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt(
				$ch,
				CURLOPT_HTTPHEADER,
				array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string)
				)
			);
			// Eksekusi cURL
			$result = curl_exec($ch);
			$memberImage = json_decode($result, true);
		}

		$this->load->view('dashboard/member_search', [
			'title' => 'Member Search',
			'members' => $this->db->get('members')->result_array(),
			'dataSearch' => $memberImage,
			'devices' => $this->device_model->get_devices()
		]);
	}

	public function member_search_post()
	{
		// $nama_member = $this->input->get('nama_member');
		// $id_device = $this->input->get('id_device');
		// $id_member = $this->input->get('id_member');
		$nama_member = '0008413137';
		$id_device = 18;
		$id_member = 143517;

		$device = $this->db->get_where('devicefacerecognition', ['id' => $id_device])->row_array();
		$data_string = json_encode([
			"operator" => "SearchPersonList",
			"info" => array(
				"DeviceID" => 1935378,
				"Picture" => 1,
				"Name" => $nama_member,
				"CustomizeID" => 145229,
				"PersonUUID" => 145229,
			),
		], JSON_UNESCAPED_SLASHES);
		// URL tujuan API
		$url =  $device['ip_address'] . '/action/SearchPersonList';
		// Inisialisasi cURL
		$ch = curl_init($url);
		// Set auth basic
		curl_setopt($ch, CURLOPT_USERPWD, "admin:admin");
		// Set method POST dan tambahkan data yang akan dikirimkan
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		// Eksekusi cURL
		$result = curl_exec($ch);
		$memberImage = json_decode($result, true);

		var_dump($memberImage);
		die;
	}

	public function index()
	{
		$this->load->view('dashboard/index', [
			'title' => 'Dashboard',
			'in_recognition' => count($this->db->get_where('log_report', ['type' => 'in', 'device' => 'Face Recognizing', 'DATE(time)' => date('Y-m-d')])->result_array()),
			'out_recognition' => count($this->db->get_where('log_report', ['type' => 'out', 'device' => 'Face Recognizing', 'DATE(time)' => date('Y-m-d')])->result_array()),
			'in_rfid' => count($this->db->get_where('log_report', ['type' => 'in', 'device' => 'RFID', 'DATE(time)' => date('Y-m-d')])->result_array()),
			'out_rfid' => count($this->db->get_where('log_report', ['type' => 'out', 'device' => 'RFID', 'DATE(time)' => date('Y-m-d')])->result_array()),
		]);
	}
	public function member()
	{

		$data = [
			'title' => 'Members',
			'members' => $this->db->get('members')->result_array(),
			'devices' => $this->device_model->get_devices()
		];
		$this->load->view('dashboard/member/index', $data);
	}
	public function member_create()
	{

		$data = [
			'title' => 'members',
			'member_tgyms' => $this->load->database('db2', TRUE)->get('gymmember')->result_array()
		];
		$this->load->view('dashboard/member/create', $data);
	}

	public function member_store()
	{

		$this->form_validation->set_rules('member', 'Member', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->member_create();
		} else {
			// Simpan file ke direktori
			if ($this->db->get_where('members', ['memberid' => explode('||', $this->input->post('member'))[0]])->row_array()) {
				$this->session->set_flashdata('error', 'User Sudah Ditambahkan!');
				redirect('dashboard/member');
			}
			if ($_POST['pilihan'] == 'webcam') {
				// Ambil gambar dari webcam
				$gambar = $_POST['foto_webcam'];
				$nama_file = 'webcam_' . time() . '.jpeg';
				file_put_contents('./assets/img/uploads/members/' . $nama_file, file_get_contents($gambar));
				$file_name = 'members/' . $nama_file;
			} else {
				// Ambil gambar dari upload file
				$gambar = $_FILES['foto']['tmp_name'];
				$target_dir = './assets/img/uploads/members/';
				$target_file = $target_dir . basename($_FILES["foto"]["name"]);
				move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file);
				// Ubah nama file
				$nama_file = time() . '_' . basename($_FILES["foto"]["name"]);
				$file_name = 'members/' . $nama_file;
				rename($target_file, $target_dir . $nama_file);
			}
			$this->db->insert('members', [
				'memberid' => explode('||', $this->input->post('member'))[0],
				'nama' => explode('||', $this->input->post('member'))[1],
				'photo' => $file_name,
			]);
			$this->session->set_flashdata('success', 'User Berhasil Ditambahkan!');
			redirect('dashboard/member');
		}
	}
	public function member_edit($id_member)
	{
		$data = [
			'title' => 'Members',
			'member' => $this->db->get_where('members', ['id_member' => $id_member])->row_array(),
			// 'member_tgyms' => $this->load->database('db2', TRUE)->get('gymmember')->result_array()
		];
		$this->load->view('dashboard/member/edit', $data);
	}
	public function member_delete($id_member)
	{

		$member = $this->db->get_where('members', ['id_member' => $id_member])->row_array();
		$this->db->where('id_member', $id_member)->delete('members');
		unlink('assets/img/uploads/' . $member['photo']);
		$this->session->set_flashdata('success', 'User Berhasil Dihapus!');
		redirect('/dashboard/member');
	}

	public function export_member()
	{
		$deviceTarget = $this->db->get_where('devicefacerecognition', ['id' => $this->input->post('id_device')])->row_array();

		$personInfo = [];

		// Create new array
		$newArray = [
			"operator" => "AddPersons",
			"DeviceID" => $deviceTarget['deviceid'],
			"Total" => count($this->input->post('memberid')),
		];
		// Iterate through original array
		foreach ($this->input->post('memberid') as $i => $person) {
			$member = $this->db->get_where('members', ['memberid' => $person])->row_array();
			$url_gambar = base_url('assets/img/uploads/' . $member['photo']);

			$path_gambar = FCPATH . 'assets/img/uploads/' . $member['photo'];

			$gambar = file_get_contents($path_gambar);
			$gambar_format = 'data:' . get_mime_by_extension($member['photo']) . ';base64,' . base64_encode($gambar);
			// $id = explode('-', $member['memberid'])[1];
			$personInfo = [
				"Name" => $member["nama"],
				"CustomizeID" => intval($member['id_member']),
				"PersonUUID" => $member['id_member'],
				"picinfo" => $gambar_format
			];
			// Add personInfo to newArray
			$newArray["Personinfo_$i"] = $personInfo;
		}


		// Encode newArray to json format
		$json = json_encode($newArray, JSON_UNESCAPED_SLASHES);


		$headers = array(
			"Authorization: Basic " . base64_encode("admin:admin"),
			'Content-Type: application/x-www-form-urlencoded'
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $deviceTarget['ip_address'] . '/action/AddPersons');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = json_decode(curl_exec($ch), true);
		curl_close($ch);
		// Tutup cURL

		if ($result['code'] == 200) {

			$this->session->set_flashdata('success', 'Foto Member Berhasil Ditambahkan!');
		} else {
			$this->session->set_flashdata('error', 'Foto Member Gagal Ditambahkan! ' . $result['info']['Detail']);
		}
		redirect('/dashboard/member');
	}

	public function member_update()
	{
		$dataMember = $this->db->get_where('members', ['id_member' => $this->input->post('id_member')])->row_array();
		$this->form_validation->set_rules('pilihan', 'Pilihan', 'required|trim');
		if ($this->form_validation->run() == false) {
			$this->member_edit($this->input->post('id_member'));
		} else {


			if (isset($_POST['pilihan']) && $this->input->post('pilihan') == 'webcam') {
				// Ambil gambar dari webcam
				$gambar = $_POST['foto_webcam'];
				$nama_file = 'webcam_' . time() . '.jpeg';
				file_put_contents('./assets/img/uploads/members/' . $nama_file, file_get_contents($gambar));

				$file_name = 'members/' . $nama_file;
				unlink('assets/img/uploads/' . $dataMember['photo']);
			} else if (isset($_POST['pilihan']) && $this->input->post('pilihan') == 'upload') {
				// Ambil gambar dari upload file
				$gambar = $_FILES['foto']['tmp_name'];
				$target_dir = './assets/img/uploads/members/';
				$target_file = $target_dir . basename($_FILES["foto"]["name"]);
				move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file);
				// Ubah nama file
				$nama_file = time() . '_' . basename($_FILES["foto"]["name"]);
				$file_name = 'members/' . $nama_file;
				rename($target_file, $target_dir . $nama_file);
				unlink('assets/img/uploads/' . $dataMember['photo']);
			} else {
				$file_name = $dataMember['photo'];
			}
			$this->db->update('members', [
				'photo' => $file_name,
			], ['id_member' => $this->input->post('id_member')]);
			$this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
			redirect('/dashboard/member');
		}
	}


	public function admin()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$data = [
			'title' => 'Admins',
			'admins' => $this->admin_model->get_admins()
		];
		$this->load->view('dashboard/admin/index', $data);
	}

	public function admin_create()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$data = [
			'title' => 'Admins',
		];
		$this->load->view('dashboard/admin/create', $data);
	}

	public function admin_store()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('role', 'Role', 'required|trim');
		$this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[admin_face.username]');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[admin_face.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]|max_length[12]');
		if ($this->form_validation->run() == false) {
			$this->admin_create();
		} else {

			$config['upload_path']          = './assets/img/uploads/users';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 10000;
			$config['max_width']            = 10000;
			$config['max_height']           = 10000;

			$this->load->library('upload', $config);
			$this->upload->do_upload('photo');
			$this->admin_model->insert([
				'name' => $this->input->post('name'),
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'role' => $this->input->post('role'),
				'photo' => 'users/' . $this->upload->data('file_name'),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			]);
			$this->session->set_flashdata('success', 'User Berhasil Ditambahkan!');
			redirect('dashboard/admin');
		}
	}


	public function admin_edit($id_admin_face)
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$data = [
			'title' => 'Admins',
			'admin' => $this->admin_model->get_admin('id_admin_face', $id_admin_face)[0]
		];
		$this->load->view('dashboard/admin/edit', $data);
	}

	public function admin_update()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$user = $this->admin_model->get_admin('id_admin_face', $this->input->post('id_admin_face'))[0];
		if ($this->input->post('username') == $user['username']) {
			$username_rules = 'required|trim';
		} else {
			$username_rules = 'required|trim|is_unique[admin_face.username]';
		}
		if ($this->input->post('email') == $user['email']) {
			$email_rules = 'required|trim';
		} else {
			$email_rules = 'required|trim|is_unique[admin_face.email]';
		}

		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('role', 'Role', 'required|trim');
		$this->form_validation->set_rules('username', 'Username', $username_rules);
		$this->form_validation->set_rules('email', 'Email', $email_rules);

		if ($this->form_validation->run() == false) {
			$this->admin_edit($this->input->post('id_admin_face'));
		} else {
			$config['upload_path']          = './assets/img/uploads/users';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 10000;
			$config['max_width']            = 10000;
			$config['max_height']           = 10000;

			$this->load->library('upload', $config);
			$this->upload->do_upload('photo');

			if ($this->upload->data('file_name') == '') {
				$photo = $user['photo'];
			} else {
				unlink('assets/img/uploads/' . $user['photo']);
				$photo = 'users/' . $this->upload->data('file_name');
			}
			if ($this->input->post('username') == $this->session->userdata('username')) {
				$this->session->set_userdata([
					'photo' => $photo,
				]);
			}

			if ($this->input->post('password') == '') {
				$password = $user['password'];
			} else {
				$password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			}

			$this->admin_model->update($this->input->post('id_admin_face'), [
				'name' => $this->input->post('name'),
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'role' => $this->input->post('role'),
				'photo' => $photo,
				'password' => $password,
			]);

			$this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
			redirect('/dashboard/admin');
		}
	}

	public function admin_delete($id_admin_face)
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$user = $this->admin_model->get_admin('id_admin_face', $id_admin_face)[0];

		$this->admin_model->delete($id_admin_face);
		unlink('assets/img/uploads/' . $user['photo']);
		$this->session->set_flashdata('success', 'User Berhasil Dihapus!');
		redirect('/dashboard/admin');
	}

	public function memberimage()
	{
		$device = $this->device_model->get_device('master', 1);

		$data_string = json_encode([
			"operator" => "SearchPersonList",
			"info" => array(
				"DeviceID" => $device[0]['deviceid'],
				"Picture" => 1,
				"RequestCount" => 10000,
			),
		], JSON_UNESCAPED_SLASHES);

		// Inisialisasi cURL
		$ch = curl_init($device[0]['ip_address'] . '/action/SearchPersonList');
		// Set auth basic
		curl_setopt($ch, CURLOPT_USERPWD, "admin:admin");
		// Set method POST dan tambahkan data yang akan dikirimkan
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		// Eksekusi cURL
		$result = curl_exec($ch);
		$memberImages = json_decode($result, true);


		if (isset($memberImages['info']) && isset($memberImages['info']['List'])) {
			$memberImages = $memberImages['info']['List'];
		} else {
			$memberImages = [];
		}
		$this->load->view('dashboard/memberimage/index', [
			'title' => 'Member Images',
			'memberImages' => $memberImages
		]);
	}

	public function memberimage_create()
	{
		$result = $this->load->database('db2', TRUE)->get('gymmember')->result_array();

		$this->load->view('dashboard/memberimage/create', [
			'title' => 'Member Images',
			'members' => $result
		]);
	}
	public function memberirmage_store()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			if ($_POST['pilihan'] == 'webcam') {
				// Ambil gambar dari webcam
				$gambar = $_POST['foto_webcam'];
			} else {
				// Ambil gambar dari upload file
				$gambar = $_FILES['foto']['tmp_name'];
			}

			// Baca file gambar
			$gambar = file_get_contents($gambar);

			var_dump($gambar);
			die;
		}
	}

	public function memberimage_store()
	{
		$device = $this->device_model->get_device('master', 1);

		$this->form_validation->set_rules('id_member', 'Name', 'required|trim');
		if ($this->form_validation->run() == false) {
			$this->memberimage_create();
		} else {

			$id_member = $this->input->post('id_member');

			$member = $this->load->database('db2', TRUE)->get_where('gymmember', ['id' => $id_member])->row_array();
			if ($_POST['pilihan'] == 'webcam') {
				// Ambil gambar dari webcam
				$gambar = $_POST['foto_webcam'];
			} else {
				// Ambil gambar dari upload file
				$gambar = $_FILES['foto']['tmp_name'];
			}
			// Baca file gambar
			$gambar = file_get_contents($gambar);
			// Encode gambar ke base64
			$gambar_format = "data:image/jpeg;base64," . base64_encode($gambar);

			// $id = explode('-', $member['id'])[1];
			$data = array(
				"operator" => "AddPerson",
				"info" => array(
					"DeviceID" => $device[0]['deviceid'],
					"Name" => $member['nama'],
					"CustomizeID" => intval($member['id_member']),
					"PersonUUID" => $member['id_member'],
					"Birthday" => $member['tgllahir']
				),
				"picinfo" => $gambar_format
			);


			$data_string = json_encode($data, JSON_UNESCAPED_SLASHES);

			// URL tujuan API
			$url =  $device[0]['ip_address'] . '/action/AddPerson';

			// Inisialisasi cURL
			$ch = curl_init($url);

			// Set auth basic
			curl_setopt($ch, CURLOPT_USERPWD, "admin:admin");

			// Set method POST dan tambahkan data yang akan dikirimkan
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt(
				$ch,
				CURLOPT_HTTPHEADER,
				array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string)
				)
			);
			// Eksekusi cURL
			$result = json_decode(curl_exec($ch), true);
			// Tutup cURL
			curl_close($ch);


			if ($result['code'] == 200) {
				$this->session->set_flashdata('success', 'Foto Member Berhasil Ditambahkan!');
			} else {
				$this->session->set_flashdata('error', 'Foto Member Gagal Ditambahkan! ' . $result['info']['Detail']);
			}
			redirect('/dashboard/memberimage');
		}
	}

	public function memberimage_edit($id_memberimage, $name)
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$device = $this->device_model->get_device('master', 1);

		$data = array(
			"operator" => "SearchPersonList",
			"info" => array(
				"DeviceID" => $device[0]['deviceid'],
				"Picture" => 1,
				"RequestCount" => 1,
				"Name" => urldecode($name),
				"CustomizeID" => [$id_memberimage],
				"PersonUUID" => [$id_memberimage]
			),
		);
		$data_string = json_encode($data, JSON_UNESCAPED_SLASHES);
		// URL tujuan API
		$url =  $device[0]['ip_address'] . '/action/SearchPersonList';
		// Inisialisasi cURL
		$ch = curl_init($url);
		// Set auth basic
		curl_setopt($ch, CURLOPT_USERPWD, "admin:admin");
		// Set method POST dan tambahkan data yang akan dikirimkan
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		// Eksekusi cURL
		$result = curl_exec($ch);
		$memberImage = json_decode($result, true);
		$result = $this->load->database('db2', TRUE)->get('gymmember')->result_array();

		$this->load->view('dashboard/memberimage/edit', [
			'title' => 'Member Images',
			'memberImage' => $memberImage['info']['List'][0],
			// 'members' => $this->member_model->get_members()
			'members' => $result
		]);
	}
	public function memberimage_update()
	{
		$device = $this->device_model->get_device('master', 1);

		$this->form_validation->set_rules('id_member', 'Name', 'required|trim');
		if ($this->form_validation->run() == false) {
			$this->memberimage_edit($this->input->post('CustomizeID'));
		} else {
			$id_member = $this->input->post('id_member');

			$member = $this->load->database('db2', TRUE)->get_where('gymmember', ['id' => $id_member])->row_array();

			// if ($_FILES['photo']['tmp_name']) {
			// 	$gambar = $_FILES['photo']['tmp_name'];
			// 	// Baca file gambar
			// 	$file = fopen($gambar, "rb");
			// 	$gambar = fread($file, filesize($gambar));
			// 	fclose($file);

			// 	// Encode gambar ke base64
			// 	$gambar_format = "data:image/jpeg;base64," . base64_encode($gambar);
			// }
			if ($_POST['pilihan'] == 'webcam') {
				// Ambil gambar dari webcam
				$gambar = $_POST['foto_webcam'];
				$gambar = file_get_contents($gambar);
				$gambar_format = "data:image/jpeg;base64," . base64_encode($gambar);
			} else {
				// Ambil gambar dari upload file
				$gambar = $_FILES['foto']['tmp_name'];
				$gambar = file_get_contents($gambar);
				$gambar_format = "data:image/jpeg;base64," . base64_encode($gambar);
			}

			// Baca file gambar
			// Encode gambar ke base64
			$data = array(
				"operator" => "EditPerson",
				"info" => array(
					"DeviceID" => $device[0]['deviceid'],
					"IdType" => 0,
					"CustomizeID" => $this->input->post('CustomizeID'),
					"PersonUUID" => $this->input->post('PersonUUID'),
				),
				"picinfo" => $gambar_format
			);
			$data_string = json_encode($data, JSON_UNESCAPED_SLASHES);

			// URL tujuan API
			$url =  $device[0]['ip_address'] . '/action/EditPerson';

			// Inisialisasi cURL
			$ch = curl_init($url);

			// Set auth basic
			curl_setopt($ch, CURLOPT_USERPWD, "admin:admin");

			// Set method POST dan tambahkan data yang akan dikirimkan
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt(
				$ch,
				CURLOPT_HTTPHEADER,
				array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string)
				)
			);
			// Eksekusi cURL
			$result = json_decode(curl_exec($ch), true);
			// Tutup cURL
			curl_close($ch);
			if ($result['code'] == 200) {
				$this->session->set_flashdata('success', 'Foto Member Berhasil Diubah!');
			} else {
				$this->session->set_flashdata('error', 'Foto Member Gagal Diubah!');
			}
			redirect('/dashboard/memberimage');
		}
	}

	public function memberimage_delete($id_memberimage)
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$device = $this->device_model->get_device('master', 1);

		$data = array();

		$data_string = json_encode($data, JSON_UNESCAPED_SLASHES);

		// URL tujuan API
		$url =  $device[0]['ip_address'] . '/action/DeletePerson';

		// Inisialisasi cURL
		$ch = curl_init($url);

		// Set auth basic
		curl_setopt($ch, CURLOPT_USERPWD, "admin:admin");

		// Set method POST dan tambahkan data yang akan dikirimkan
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		// Eksekusi cURL
		$result = json_decode(curl_exec($ch), true);

		// Tutup cURL
		curl_close($ch);
		if ($result['code'] == 200) {
			$this->session->set_flashdata('success', 'Foto Member Berhasil Dihapus!');
		} else {
			$this->session->set_flashdata('error', 'Foto Member Gagal Dihapus! ' . $result['info']['Detail']);
		}

		redirect('/dashboard/memberimage');
	}

	public function device()
	{
		// if ($this->session->userdata('role') !== 'admin') {
		// 	show_404();
		// }
		$data = [
			'title' => 'Devices',
			'devices' => $this->device_model->get_devices()
		];
		$this->load->view('/dashboard/device/index', $data);
	}

	public function device_create()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$data = [
			'title' => 'Devices',
		];
		$this->load->view('dashboard/device/create', $data);
	}

	public function device_store()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$this->form_validation->set_rules('nama', 'Device', 'required|trim');
		$this->form_validation->set_rules('gate', 'Gate', 'required|trim');
		$this->form_validation->set_rules('ip_address', 'IP Address', 'required|trim');
		$this->form_validation->set_rules('deviceid', 'ID Device', 'required|trim|is_unique[devicefacerecognition.deviceid]');
		if ($this->form_validation->run() == false) {
			$this->device_create();
		} else {
			$this->device_model->insert([
				'deviceid' => $this->input->post('deviceid'),
				'nama' => $this->input->post('nama'),
				'gate' => $this->input->post('gate'),
				'status_duplicate' => 0,
				'ip_address' => $this->input->post('ip_address'),
			]);
			$log = $this->log_model->get_log('id_log', $this->input->post('deviceid'));
			if (count($log) == 0) {
				$this->log_model->insert([
					'id_log' => $this->input->post('deviceid'),
				]);
			}
			$this->session->set_flashdata('success', 'Devices Berhasil Ditambahkan!');
			redirect('/dashboard/device');
		}
	}

	public function device_edit($id_device)
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$data = [
			'title' => 'Devices',
			'device' => $this->device_model->get_device('id', $id_device)[0]
		];
		$this->load->view('/dashboard/device/edit', $data);
	}

	public function device_update()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$device = $this->device_model->get_device('id', $this->input->post('id_device'))[0];

		if ($this->input->post('deviceid') == $device['deviceid']) {
			$device_rules = 'required|trim';
		} else {
			$device_rules = 'required|trim|is_unique[devicefacerecognition.deviceid]';
		}
		$this->form_validation->set_rules('deviceid', 'ID Device', $device_rules);
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('gate', 'Gate', 'required|trim');
		if ($this->form_validation->run() == false) {
			$this->device_edit($this->input->post('id_device'));
		} else {
			$this->device_model->update($this->input->post('id_device'), [
				'deviceid' => $this->input->post('deviceid'),
				'nama' => $this->input->post('nama'),
				'gate' => $this->input->post('gate'),
				'ip_address' => $this->input->post('ip_address'),
			]);
			$this->log_model->update($device['deviceid'], [
				'id_log' => $this->input->post('deviceid'),
			]);
			$this->session->set_flashdata('success', 'Devices Berhasil Diperbarui!');
			redirect('/dashboard/device');
		}
	}

	public function device_update_master()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$device = $this->device_model->get_device('master', 1);
		if (count($device) != 0) {
			$this->device_model->update($device[0]['id'], [
				'master' => 0,
			]);
		}
		$this->device_model->update($this->input->post('id_device'), [
			'master' => 1,
		]);
		$this->session->set_flashdata('success', 'Devices Berhasil Diperbarui!');
		redirect('/dashboard/device');
	}

	public function device_delete($id_device)
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}
		$device = $this->device_model->get_device('id', $id_device)[0];
		$this->device_model->delete($id_device);
		$this->log_model->delete($device['deviceid']);
		$this->session->set_flashdata('success', 'Devices Berhasil Dihapus!');
		redirect('dashboard/device');
	}


	public function deleteBatch($deviceTarget)
	{
		// Delete All Images -  selected device
		$deleteData = json_encode([
			"operator" => "DeleteAllPerson",
			"info" => array(
				"DeleteAllPersonCheck" => 1,
			),
		], JSON_UNESCAPED_SLASHES);
		// Inisialisasi cURL
		$chDelete = curl_init($deviceTarget[0]['ip_address'] . '/action/DeleteAllPerson');
		// Set auth basic
		curl_setopt($chDelete, CURLOPT_USERPWD, "admin:admin");
		// Set method POST dan tambahkan data yang akan dikirimkan
		curl_setopt($chDelete, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($chDelete, CURLOPT_POSTFIELDS, $deleteData);
		curl_setopt($chDelete, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(
			$chDelete,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($deleteData)
			)
		);
		// Eksekusi cURL
		$result = curl_exec($chDelete);
		$resultDelete = json_decode($result, true);
		curl_close($chDelete);
	}

	public function emptyImage($deviceId)
	{
		$deviceTarget = $this->device_model->get_device('deviceid', $deviceId);
		$username = "admin";
		$password = "admin";
		$headers = array(
			"Authorization: Basic " . base64_encode("$username:$password"),
			'Content-Type: application/x-www-form-urlencoded'
		);
		$deleteData = json_encode([
			"operator" => "DeleteAllPerson",
			"info" => array(
				"DeleteAllPersonCheck" => 1,
			),
		], JSON_UNESCAPED_SLASHES);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $deviceTarget[0]['ip_address'] . '/action/DeleteAllPerson');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $deleteData);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$res = json_decode(curl_exec($ch), true);
		curl_close($ch);
		$this->device_model->update($deviceTarget[0]['id'], [
			'status_duplicate' => 1
		]);

		$this->session->set_flashdata('success-refresh', 'Foto Member Berhasil Dihapus!');

		redirect('/dashboard/device');
	}

	public function export_images($deviceId)
	{
		$deviceTarget = $this->device_model->get_device('deviceid', $deviceId);
		$deviceMaster = $this->device_model->get_device('master', 1);
		$username = "admin";
		$password = "admin";
		$headers = array(
			"Authorization: Basic " . base64_encode("$username:$password"),
			'Content-Type: application/x-www-form-urlencoded'
		);
		$options = array(
			'http' => array(
				'header'  => $headers,
				'method'  => 'POST',
				'content' => json_encode([
					"operator" => "DeleteAllPerson",
					"info" => array(
						"DeleteAllPersonCheck" => 1,
					),
				], JSON_UNESCAPED_SLASHES),
			),
		);

		$context  = stream_context_create($options);
		$deleteData = json_encode([
			"operator" => "DeleteAllPerson",
			"info" => array(
				"DeleteAllPersonCheck" => 1,
			),
		], JSON_UNESCAPED_SLASHES);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $deviceTarget[0]['ip_address'] . '/action/DeleteAllPerson');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $deleteData);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// curl_exec($ch);
		curl_close($ch);
		$getImageData = json_encode([
			"operator" => "SearchPersonList",
			"info" => array(
				"DeviceID" => $deviceMaster[0]['deviceid'],
				"Picture" => 1,
				"RequestCount" => 10000,
			),
		], JSON_UNESCAPED_SLASHES);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $deviceMaster[0]['ip_address'] . '/action/SearchPersonList');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $getImageData);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$resultGetImage = curl_exec($ch);
		curl_close($ch);

		$masterImages = json_decode($resultGetImage, true);
		$personInfo = [];
		$originalArray = $masterImages['info'];
		// Create new array
		$newArray = [
			"operator" => "AddPersons",
			"DeviceID" => $deviceTarget[0]['deviceid'],
			"Total" => $originalArray["Listnum"],
		];
		// Iterate through original array
		foreach ($originalArray["List"] as $i => $person) {
			$personInfo = [
				"Name" => $person["Name"],
				"CustomizeID" => $person["CustomizeID"],
				"PersonUUID" => $person["PersonUUID"],
				"picinfo" => $person["Picinfo"]
			];
			// Add personInfo to newArray
			$newArray["Personinfo_$i"] = $personInfo;
		}
		// Encode newArray to json format
		$json = json_encode($newArray, JSON_UNESCAPED_SLASHES);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $deviceTarget[0]['ip_address'] . '/action/AddPersons');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = json_decode(curl_exec($ch), true);
		curl_close($ch);
		// Tutup cURL
		if ($result['code'] == 200) {
			$this->device_model->update($deviceTarget[0]['id'], [
				'status_duplicate' => 0
			]);
			$this->session->set_flashdata('success', 'Foto Member Berhasil Ditambahkan!');
		} else {
			$this->session->set_flashdata('error', 'Foto Member Gagal Ditambahkan! ' . $result['info']['Detail']);
		}
		redirect('/dashboard/device');
	}

	public function log_report()
	{
		if ($this->session->userdata('role') !== 'admin') {
			show_404();
		}

		if ($this->input->get('startDate') && $this->input->get('endDate')) {
			$log_reports = $this->db->where('time >=', $this->input->get('startDate'));
			$log_reports = $this->db->where('time <', date('Y-m-d', strtotime($this->input->get('endDate') . ' +1 day')));
		}

		// if ($this->input->get('startDate') && $this->input->get('endDate')) {
		// 	$log_reports = $this->db->where('time >=', $this->input->get('startDate'));
		// 	$log_reports = $this->db->where('time <=', $this->input->get('endDate') + 1);
		// }
		if ($this->input->get('member_type')) {
			if ($this->input->get('member_type') == 'member') {
				$log_reports = $this->db->where('device', 'Face Recognizing');
			} else if ($this->input->get('member_type') == 'visit') {
				$log_reports = $this->db->where('device', 'RFID');
			}
		}
		$log_reports = $this->db->order_by('time', 'desc');
		$log_reports = $this->db->get('log_report')->result_array();


		$this->load->view('dashboard/log_report/index', [
			'title' => 'Log Report',
			'log_reports' => $log_reports
		]);
	}

	public function zip()
	{

		$zip = new ZipArchive();

		$filename = 'file.zip';

		if ($zip->open($filename, ziparchive::CREATE) !== TRUE) {
			exit("cannot open <$filename>\n");
		}

		$zip->addFromString('file.txt', 'Hi there, it is file.txt'); // add new raw file
		$zip->addFile("path-to/file.txt", "/file.php"); // to add current file

		// close and save archive
		$zip->close();

		// download file
		if (file_exists($filename)) {
			header('Content-Type: application/zip');
			header('Content-Length: ' . filesize($filename));

			// download zip
			readfile($filename);

			// delete after download
			unlink($filename);
		}
	}

	public function download_export()
	{
		$data = [
			'title' => 'Members',
			'members' => $this->db->get('members')->result_array()
		];
		$this->load->view('dashboard/member/export', $data);
	}

	public function download_images()
	{
		$members = $this->db->get('members')->result_array();

		$zip = new ZipArchive();
		$zip_name = "Picture.zip";

		if ($zip->open($zip_name, ZipArchive::CREATE | ZipArchive::OVERWRITE) !== TRUE) {
			exit("Cannot create $zip_name\n");
		}

		foreach ($members as $key => $member) {
			$imageUrl = str_replace(' ', '%20', $member['photo']);
			$image_url = base_url('assets/img/uploads/' . $imageUrl);

			$filename = basename($image_url);
			$filegetcontents = file_get_contents($image_url);
			$new = urldecode($filegetcontents); // Ubah "%20" menjadi spasi

			$zip->addFromString($filename, $new);
			die;
		}

		$zip->close();

		header("Content-type: application/zip");
		header("Content-Disposition: attachment; filename=$zip_name");
		header("Pragma: no-cache");
		header("Expires: 0");
		readfile("$zip_name");
		exit;
	}

	public function download_image_sample()
	{
		$zipFilename = 'archive.zip';
		$imageUrl = 'http://localhost:80/tgym/assets/img/uploads/members/1681540591_Antonius%20Gatot%20Kusnadi.bmp';

		// Ekstrak nama file dari URL
		$filename = basename($imageUrl);

		// Tentukan direktori tujuan penyimpanan gambar di XAMPP
		$destination = 'C:/xampp/htdocs/tgym/assets/img/uploads/members/1681540591_Antonius%20Gatot%20Kusnadi.bmp';

		// Unduh gambar menggunakan copy()
		if (copy($imageUrl, $destination)) {
			// Inisialisasi objek ZipArchive
			$zip = new ZipArchive();

			// Buka arsip ZIP untuk ditulis
			if ($zip->open($zipFilename, ZipArchive::CREATE) === true) {
				// Masukkan gambar ke dalam arsip ZIP dengan nama file yang sesuai
				$zip->addFile($destination, $filename);

				// Tutup arsip ZIP
				$zip->close();

				echo "Gambar berhasil diunduh dan dimasukkan ke dalam arsip ZIP.";
			} else {
				echo "Gagal membuka atau membuat arsip ZIP.";
			}
		} else {
			echo "Gagal mengunduh gambar.";
		}
	}

	public function setting()
	{
		$data = [
			'title' => 'Setting',
			'settings' => $this->db->get('setting')->result()
		];
		$this->load->view('dashboard/setting', $data);
	}

	public function setting_update()
	{
	
		$expire_sound_config = [
			'upload_path' => './assets/music/uploads/setting',
			'max_size' => 50240, // Ukuran maksimum file (dalam kilobita).
			'allowed_types' => '*', // Mengizinkan semua jenis musik.
		];
	
		$this->load->library('upload', $expire_sound_config);
	
	
	
		if ($this->upload->do_upload('expire_sound')) {
			$expire_sound_upload_data = $this->upload->data();
			$new_expire_sound_file = $expire_sound_upload_data['file_name'];
			$this->db->update('setting', ['value' => $new_expire_sound_file], ['key' => 'expire_sound']);

			$previous_expire_sound = $this->db->get_where('setting', ['key' => 'expire_sound'])->row_array();
			$previous_expire_sound_file = $previous_expire_sound['value'];
			if (!empty($previous_expire_sound_file) && file_exists('./assets/music/uploads/setting/' . $previous_expire_sound_file)) {
				unlink('./assets/music/uploads/setting/' . $previous_expire_sound_file);
			}
		} else {
			$expire_sound_error = $this->upload->display_errors();
		}
	
		$active_sound_config = [
			'upload_path' => './assets/music/uploads/setting',
			'max_size' => 50240, // Ukuran maksimum file (dalam kilobita).
			'allowed_types' => '*', // Mengizinkan semua jenis musik.
		];
	
		$this->load->library('upload', $active_sound_config);
	
		if ($this->upload->do_upload('active_sound')) {
			$active_sound_upload_data = $this->upload->data();
			$new_active_sound_file = $active_sound_upload_data['file_name'];
			$this->db->update('setting', ['value' => $new_active_sound_file], ['key' => 'active_sound']);

			$previous_active_sound = $this->db->get_where('setting', ['key' => 'active_sound'])->row_array();
			$previous_active_sound_file = $previous_active_sound['value'];
			if (!empty($previous_active_sound_file) && file_exists('./assets/music/uploads/setting/' . $previous_active_sound_file)) {
				unlink('./assets/music/uploads/setting/' . $previous_active_sound_file);
			}
		} else {
			$active_sound_error = $this->upload->display_errors();
		}
	
		$unrecognize_sound_config = [
			'upload_path' => './assets/music/uploads/setting',
			'max_size' => 50240, 
			'allowed_types' => '*', 
		];
	
		$this->load->library('upload', $unrecognize_sound_config);
	
		
	
		if ($this->upload->do_upload('unrecognize_sound')) {
			$unrecognize_sound_upload_data = $this->upload->data();
			$new_unrecognize_sound_file = $unrecognize_sound_upload_data['file_name'];
			$this->db->update('setting', ['value' => $new_unrecognize_sound_file], ['key' => 'unrecognize_sound']);

			$previous_unrecognize_sound = $this->db->get_where('setting', ['key' => 'unrecognize_sound'])->row_array();
		$previous_unrecognize_sound_file = $previous_unrecognize_sound['value'];
		if (!empty($previous_unrecognize_sound_file) && file_exists('./assets/music/uploads/setting/' . $previous_unrecognize_sound_file)) {
			unlink('./assets/music/uploads/setting/' . $previous_unrecognize_sound_file);
		}
		} else {
			$unrecognize_sound_error = $this->upload->display_errors();
		}
	
		redirect('dashboard/setting');
	}
	
}
