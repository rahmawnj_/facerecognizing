<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Email extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model(['memberimage_model', 'member_model', 'log_model', 'device_model', 'admin_model']);
        $this->load->helper(['form', 'url',]);
    }

    public function forgot_password()
    {
        $this->load->view('forgot_password');
    }
    public function form_password()
    {
        $user = $this->db->get_where('admin_face', ['id_admin_face' => $this->input->get('token')])->row_array();

        $this->load->view('form_password', [
            'id_admin_face' => $user['id_admin_face']
        ]);
    }

    public function reset_password()
    {
        $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]|max_length[12]');

        $this->admin_model->update($this->input->post('id_admin_face'), [
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
        ]);
        redirect('email');
    }

    public function send_password_request()
    {

        $mail             = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host       = "mail.gmail.com"; // SMTP server
        $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Host       = "smtp.gmail.com";      // SMTP server
        $mail->Port       = 587;                   // SMTP port
        $mail->Username   = "rahmawnj3@gmail.com";  // username
        $mail->Password   = "lkqqdcsygbtldqkb";            // password

        $mail->SetFrom('rahmawnj3@gmail.com', 'Test');
        $mail->Subject    = "Ubah Password!";

        $member = $this->db->get_where('admin_face', ['email' => $this->input->post('email')])->row();
        if ($member) {
            $mail->MsgHTML("
			<p>Silakan klik tautan berikut untuk mereset password akun $member->name:</p>
			<p>
			<a href='" . site_url("email/form_password?token=$member->id_admin_face") . "'>Ubah Password</a>
			</p>
			");
        } else {
            $mail->MsgHTML("Email Tidak Terdaftar");
        }

        $address = $this->input->post('email');
        $mail->AddAddress($address, "Ubah Password");

        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }

        redirect('email');
    }
    public function sendnotif()
    {
        $mail             = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host       = "mail.gmail.com"; // SMTP server
        $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Host       = "smtp.gmail.com";      // SMTP server
        $mail->Port       = 587;                   // SMTP port
        $mail->Username   = "rahmawnj3@gmail.com";  // username
        $mail->Password   = "lkqqdcsygbtldqkb";            // password

        $mail->SetFrom('rahmawnj3@gmail.com', 'Test');

        $mail->Subject    = "test!";

        // $mail->MsgHTML("<p>Silakan klik tautan berikut untuk mereset password Anda:</p>
        // <p><a href='" . site_url("email/reset_password?email=rahmawnj4@mail.com") . "'>" . site_url("email/reset_password?email=rahmawnj4@mail.com") . "</a></p>");
        $mail->MsgHTML("<p style='font-family: Arial, sans-serif; font-size: 16px;'>Silakan klik tautan berikut untuk mereset password Anda:</p>
        <p style='font-family: Arial, sans-serif; font-size: 16px;'>
            <a href='" . site_url("email/reset_password?email=rahmawnj4@mail.com") . "' style='color: #007bff; text-decoration: none;'>"
            . site_url("email/reset_password?email=rahmawnj4@mail.com") . "</a>
        </p>");

        $address = "rahmawnj4@gmail.com";
        $mail->AddAddress($address, "Test");

        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }
    }
}
