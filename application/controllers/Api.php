<?php
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(['log_model', 'device_model']);
		$this->load->helper(['form', 'url', 'database2']);
	}

	public function verify_post($id = null)
	{
		$post = json_decode($this->input->raw_input_stream);
		$device = $this->db->get_where('devicefacerecognition', ['deviceid' => $post->info->DeviceID])->row_array();


		// $user = $this->load->database('db2', TRUE)
		// 	->select('c.*, a.*, d.jenis, c.id AS id_member')  // Aliaskan kolom agar lebih mudah diakses
		// 	->from('gymtransaksi a')
		// 	->join('gymtransaksidetail b', 'a.id = b.id')
		// 	->join('gymmember c', 'a.memberid = c.id')
		// 	->join('gymkelas d', 'd.id = b.kelasid')
		// 	->where_in('d.jenis', ['Member', 'Personal Trainer', 'Visit'])
		// 	->like('c.nama', $post->info->Name)
		// 	->order_by('a.jatuhtempo', 'DESC')
		// 	->get()
		// 	->row_array();
		$user = $this->load->database('db2', TRUE)
->select('m., t., k.jenis')  // Aliaskan kolom agar lebih mudah diakses
->from('gymtransaksi t')
->join('gymtransaksidetail td', 't.id = td.id')
->join('gymmember m', 't.memberid = m.id')
->join('gymkelas k', 'k.id = td.kelasid')
->where_in('k.jenis', ['Member','Visit'])
->like('m.nama', $post->info->Name)
->where("jatuhtempo > current_date", null, false)
->order_by('t.jatuhtempo', 'DESC')
->get()
->row_array();

if ($user != null) {
	$this->db->update(
		'tripod',
		[$device['gate'] . '_gate' => 1, 'timestamp' => date('Y-m-d H:i:s')],
		['raspberryid' => 'rs1', $device['gate'] . '_gate' => $device['gate']]
	);

	$log  = $this->db->get_where('log_detection', ['id_log' => $id, 'id_card' => $post->info->CustomizeID])->row_array();

		if ($log) {
			$currentTime = time(); // get current time as UNIX timestamp
			$logTime = intval($log['time']); // convert log time string to UNIX timestamp
			if (($currentTime - $logTime) > 10) {
				$this->log_model->update($id, [
					'operator' => $post->operator,
					'id_card' => explode('-', $user['memberid'])[1],
					'time' => time(),
					'image' => $post->SanpPic,
					'id_member' => $user['memberid']
				]);
				var_dump($user['memberid']);

				$this->db->insert('log_report', [
					'memberid' => $user['memberid'],
					'nama' => $user['nama'],
					'alamat' => $user['alamat'],
					'type' => $device['gate'],
					'device' => 'Face Recognizing',
					'time' => date('Y-m-d H:i:s')
				]);
			}
		}


		if (!$log) {
			$this->log_model->update($id, [
				'operator' => $post->operator,
				'id_card' => explode('-', $user['memberid'])[1],
				'time' => time(),
				'image' => $post->SanpPic,
				'id_member' => $user['memberid']

			]);
			var_dump($user['memberid']);

			$this->db->insert('log_report', [
				'memberid' => $user['memberid'],
				'nama' => $user['nama'],
				'alamat' => $user['alamat'],
				'type' => $device['gate'],
				'device' => 'Face Recognizing',
				'time' => date('Y-m-d H:i:s')
			]);
		}
};



		// if (date('Y-m-d') <= $user['jatuhtempo']) {
		// $this->db->update('tripod', [$device['gate'] . '_gate' => 1, 'timestamp' => date('Y-m-d H:i:s')], ['raspberryid' => 'rs1', $device['gate'] . '_gate' => $device['gate']]);
		// }

		// if (date('Y-m-d') <= $user['jatuhtempo']) {
		// 	if ($user['jenis'] == 'Member') {
		// 	//	$this->db->update('tripod', [$device['gate'] . '_gate' => 1, 'timestamp' => date('Y-m-d H:i:s')], ['raspberryid' => 'rs1', $device['gate'] . '_gate' => $device['gate']]);
		// 	}
		// }

		
	}

	public function snap_post($id = null)
	{
		$post = json_decode($this->input->raw_input_stream);
		$time = time();
		$this->log_model->update($id, [
			'operator' => $post->operator,
			'id_card' => '',
			'time' => $time,
			'image' => $post->SanpPic
		]);
	}
	public function heartbeat_post($id = null)
	{
		$post = json_decode($this->input->raw_input_stream);

		$time = time();
		$this->log_model->update($id, [
			'time_heartbeat' => $time,
		]);
	}

	public function last_log_get($id = null)
	{

		$log = $this->db->get_where('log_detection', ['id_log' => $id])->row_array();

		if ($log != null) {
			$idcard = $log['id_card'];

			$data = $this->load->database('db2', TRUE)
				->select('gymmember.*, gymtransaksi.jatuhtempo, gymtransaksi.keterangan, gymtransaksi.tglberlaku')
				->from('gymmember')
				->join('gymtransaksi', 'gymtransaksi.memberid = gymmember.id')
				->where('gymmember.id', $log['id_member'])
				->order_by('gymtransaksi.jatuhtempo', 'DESC')
				->get()
				->row_array();


			if (!$data) {
				$data = $this->load->database('db2', TRUE)
					->from('gymmember')
					->where('gymmember.id', $log['id_member'])
					->get()
					->row_array();
			}

			$this->response(['msg' => 'success', 'logs' => $log, 'data_user' => $data]);
		} else {
			$this->response(['msg' => 'success', 'logs' => 'id-tidak-terdaftar', 'data_user' => '']);
		}
	}

	public function gate_get($raspberryid)
	{
		$raspberry = $this->db->get_where('tripod', ['raspberryid' => $raspberryid])->row_array();
		if ($raspberry != null) {
			$inGate = '';
			$outGate = '';
			$given_time = strtotime($raspberry['timestamp']);
			$current_time = time();
			$status = '';
			if ($current_time - $given_time > 30) {
				$status = 'success';
				$inGate = 'close';
				$outGate = 'close';
			} else {
				$status = 'success';
				if ($raspberry['in_gate'] == 1) {
					$inGate = 'open';
				} else {
					$inGate = 'close';
				}
				if ($raspberry['out_gate'] == 1) {
					$outGate = 'open';
				} else {
					$outGate = 'close';
				}
			}

			$this->db->update('tripod', ['in_gate' => 0, 'out_gate' => 0,], ['raspberryid' => 'rs1']);
			$this->response(['msg' => $status, 'id' => $raspberryid, 'status_in' => $inGate, 'status_out' => $outGate]);
		} else {
			$this->response(['msg' => 'error', 'logs' => 'raspberryid-tidak-terdaftar', 'data_user' => '']);
		}
	}

	public function barcode_in_post()
	{

		// $member = $this->load->database('db2', TRUE)->select('gymmember.*, gymtransaksi.jam')->join('gymtransaksi', 'gymtransaksi.memberid = gymmember.id')->order_by('gymtransaksi.jam', 'DESC')->get_where('gymmember', ['nama' => $this->input->post('kode')])->row_array();
		$member = $this->load->database('db2', TRUE)
			->select('c.*, a.jam, d.jenis')  // Aliaskan kolom agar lebih mudah diakses
			->from('gymtransaksi a')
			->join('gymtransaksidetail b', 'a.id = b.id')
			->join('gymmember c', 'a.memberid = c.id')
			->join('gymkelas d', 'd.id = b.kelasid')
			->where('c.nama', $this->input->post('kode'))
			->where_in('d.jenis', ['Visit'])
			->order_by('a.jam', 'DESC')
			->limit(1)
			->get()
			->row_array();

		$status = '';
		$inGate = '';
		if ($member != null) {
			$timezone = new DateTimeZone('Asia/Jakarta');
			$member_time = new DateTime($member['jam'], $timezone);
			$current_time = new DateTime('now', $timezone);
			$time_diff_in_minutes = round(($current_time->getTimestamp() - $member_time->getTimestamp()) / 60);

			if ($time_diff_in_minutes >= 90) {
				$status = 'success';
				$inGate = 'close';
			} else {
				if ($member['jenis'] == 'Visit') {
					$status = 'success';
					$inGate = 'open';

					$this->db->insert('log_report', [
						'nama' => $this->input->post('kode'),
						'type' => 'in',
						'device' => 'RFID',
						'alamat' => $member['alamat'],
						'memberid' => $member['id'],
						'time' => date('Y-m-d H:i:s')
					]);
				} else {
					$status = 'success';
					$inGate = 'close';
				}
			}
		} else {
			$status = 'error';
			$inGate = 'close';
		}

		$this->response(['msg' => $status, 'kode' => $this->input->post('kode'),  'status_in' => $inGate]);
	}
	public function barcode_out_post()
	{

		$member = $this->load->database('db2', TRUE)
			->select('c.*, a.jam, d.jenis')  // Aliaskan kolom agar lebih mudah diakses
			->from('gymtransaksi a')
			->join('gymtransaksidetail b', 'a.id = b.id')
			->join('gymmember c', 'a.memberid = c.id')
			->join('gymkelas d', 'd.id = b.kelasid')
			->where('c.nama', $this->input->post('kode'))
			->where_in('d.jenis', ['Visit'])
			->order_by('a.jam', 'DESC')
			->limit(1)
			->get()
			->row_array();

		$status = '';
		$outGate = '';

		if ($member != null) {
			$timezone = new DateTimeZone('Asia/Jakarta');
			$member_time = new DateTime($member['jam'], $timezone);
			$current_time = new DateTime('now', $timezone);
			$time_diff_in_minutes = round(($current_time->getTimestamp() - $member_time->getTimestamp()) / 60);

			if ($time_diff_in_minutes >= 90) {
				$status = 'success';
				$outGate = 'close';
			} else {
				if ($member['jenis'] == 'Visit') {
					$status = 'success';
					$outGate = 'open';
					$this->db->insert('log_report', [
						'nama' => $this->input->post('kode'),
						'type' => 'out',
						'device' => 'RFID',
						'alamat' => $member['alamat'],
						'memberid' => $member['id'],
						'time' => date('Y-m-d H:i:s')
					]);
				} else {
					$status = 'success';
					$outGate = 'close';
				}
			}
		} else {
			$status = 'error';
			$outGate = 'close';
		}

		// $this->db->update('tripod', ['out_gate' => 1, 'timestamp' => date('Y-m-d H:i:s')], ['raspberryid' => 'rs1']);
		$this->response(['msg' => $status, 'kode' => $this->input->post('kode'), 'status_out' => $outGate]);
	}

	public function barcode_out2_post()
	{
		$query = $this->load->database('db2', TRUE)->where('gymmember.nama', $this->input->post('kode'))->get('gymmember')->result();


		// $member = $this->load->database('db2', TRUE)->get_where('gymmember', ['nama' => $this->input->post('kode')])->row_array();
		// $member = $this->load->database('db2', TRUE)->select('nama, alamat, gymmember.id, gymtransaksi.jam ')->join('gymtransaksi', 'gymtransaksi.memberid = gymmember.id')->order_by('gymtransaksi.jam', 'DESC')->get_where('gymmember', ['nama' => $this->input->post('kode')])->row_array();
		// $member = $this->load->database('db2', TRUE)
		// 	->select('gymmember.*, gymtransaksi.jam')
		// 	->from('gymmember')
		// 	->join('gymtransaksi', 'gymtransaksi.memberid = gymmember.id')
		// 	->where('gymmember.nama', $this->input->post('kode'))
		// 	->order_by('gymtransaksi.jam', 'DESC')
		// 	->limit(1)
		// 	->get()
		// 	->row_array();

		$member = $this->load->database('db2', TRUE)
			->select('c.*, a.jam, d.jenis')  // Aliaskan kolom agar lebih mudah diakses
			->from('gymtransaksi a')
			->join('gymtransaksidetail b', 'a.id = b.id')
			->join('gymmember c', 'a.memberid = c.id')
			->join('gymkelas d', 'd.id = b.kelasid')
			->where('c.nama', $this->input->post('kode'))
			->where_in('d.jenis', ['Visit'])
			->order_by('a.jam', 'DESC')
			->limit(1)
			->get()
			->row_array();
		var_dump($member);
		die;



		if (date('Y-m-d') <= $member['jatuhtempo']) {
			$this->db->update('tripod', [$device['gate'] . '_gate' => 1, 'timestamp' => date('Y-m-d H:i:s')], ['raspberryid' => 'rs1', $device['gate'] . '_gate' => $device['gate']]);
		}

		$this->response([$member]);

		$status = '';
		$outGate = '';
		if ($member) {
			$given_time = strtotime($member['jam']);
			$current_time = time();
			$difference = $current_time - $given_time;
			if ($difference > (90 * 60)) {
				$status = 'success';
				$outGate = 'close';
			} else {
				$status = 'success';
				$outGate = 'open';

				$this->db->insert('log_report', [
					'nama' => $this->input->post('kode'),
					'type' => 'out',
					'device' => 'RFID',
					'alamat' => $member['alamat'],
					'memberid' => $member['id'],
					'time' => date('Y-m-d H:i:s')
				]);
			}
		} else {
			$status = 'error';
			$outGate = 'close';
		}

		// $this->db->update('tripod', ['out_gate' => 1, 'timestamp' => date('Y-m-d H:i:s')], ['raspberryid' => 'rs1']);
		$this->response(['msg' => $status, 'kode' => $this->input->post('kode'), 'status_out' => $outGate]);
	}
}
