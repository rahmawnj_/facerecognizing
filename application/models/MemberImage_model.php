<?php
class MemberImage_model extends CI_Model
{
    public function get_memberimages()
    {
        return $this->db->get('gymmemberimage')->result_array();
    }

    public function get_memberimage($field, $data)
    {
        return $this->db->get_where('gymmemberimage', [$field => $data])->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('gymmemberimage', $data[0]);
    }

    public function update($id_gymmemberimage, ...$data)
    {
        $this->db->update('gymmemberimage', $data[0], ['id_gymmemberimage' => $id_gymmemberimage]);
    }

    public function delete($id_gymmemberimage)
    {
        $this->db->delete('gymmemberimage', array('id_gymmemberimage' => $id_gymmemberimage));
    }
}
