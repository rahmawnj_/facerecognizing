<?php
class Admin_model extends CI_Model
{
    public function get_admins()
    {
        return $this->db->get('admin_face')->result_array();
    }

    public function get_admin($field, $data)
    {
        return $this->db->get_where('admin_face', [$field => $data])->result_array();
    }

    public function insert(...$data)
    { 
       return $this->db->insert('admin_face', $data[0]);
    }

    public function update($id_admin, ...$data)
    {
       return $this->db->update('admin_face', $data[0], ['id_admin_face' => $id_admin]);
    }

    public function delete( $id_admin)
    {
        return $this->db->where('id_admin_face', $id_admin)->delete('admin_face');
    }
}