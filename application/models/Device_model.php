<?php
class Device_model extends CI_Model
{
    public function get_devices()
    {
        return $this->db->get('devicefacerecognition')->result_array();
    }

    public function get_device($field, $data)
    {
        return $this->db->get_where('devicefacerecognition', [$field => $data])->result_array();
    }

    public function insert(...$data)
    { 
       return $this->db->insert('devicefacerecognition', $data[0]);
    }

    public function update($id_device, ...$data)
    {
       return $this->db->update('devicefacerecognition', $data[0], ['id' => $id_device]);
    }

    public function delete( $id_device)
    {
        return $this->db->where('id', $id_device)->delete('devicefacerecognition');
    }
}
