<?php
class Member_model extends CI_Model
{
    public function get_members()
    {
        return $this->db->get('gymmember')->result_array();
    }

    public function get_member($field, $data)
    {
        return $this->db->get_where('gymmember', [$field => $data])->result_array();
    }

}
