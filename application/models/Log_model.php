<?php
class Log_model extends CI_Model
{
    public function get_logs()
    {
        $this->db->order_by('time', 'desc');
        return $this->db->get('log_detection')->result_array();
    }
    public function get_log_by_date($date)
    {
        $this->db->order_by('time', 'desc');
        return $this->db->query("SELECT * FROM log WHERE DATE(time) = '$date'")->result_array();
    }

    public function get_log($field, $data)
    {
        $this->db->order_by('time', 'desc');
        return $this->db->get_where('log_detection', [$field => $data])->result_array();
    }

    public function insert(...$data)
    {
        return $this->db->insert('log_detection', $data[0]);
    }

    public function update($id_log, ...$data)
    {
        return $this->db->update('log_detection', $data[0], ['id_log' => $id_log]);
    }

    public function delete($id_log)
    {
        return $this->db->where('id_log', $id_log)->delete('log_detection');
    }
}