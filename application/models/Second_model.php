<?php
class Second_model extends CI_Model
{
 private $db2;

 public function __construct()
 {
  parent::__construct();
         $this->db2 = $this->load->database('db2', TRUE);
 }
 public function get_user()
 {
  return $this->db2->get('member');
 }

}