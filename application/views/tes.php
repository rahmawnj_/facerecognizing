<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <div id="accordionContainer"></div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <script>
      document.addEventListener("DOMContentLoaded", function() {
        var apiUrl = "https://bimbelpro.com/explain-answer?user_id=82a69229-f9bb-44df-9258-f97b8bac242d&package_id=41076d1c-0937-49e0-832f-9813c5abed32";
        
        var accordionItems = [
          "Accordion 1 Content",
          "Accordion 2 Content",
          "Accordion 3 Content",
          "Accordion 4 Content",
          "Accordion 5 Content",
          "Accordion 6 Content",
          "Accordion 7 Content",
          "Accordion 8 Content",
          "Accordion 9 Content",
          "Accordion 10 Content"
        ];

        // Create accordions dynamically
        var accordionContainer = document.getElementById("accordionContainer");

        accordionItems.forEach(function(item, index) {
          var accordionId = "accordion" + (index + 1);

          // Create accordion card
          var accordionCard = document.createElement("div");
          accordionCard.classList.add("card");
          accordionCard.style.marginTop = "10px"; // Add margin-top of -3

          // Create card header
          var cardHeader = document.createElement("div");
          cardHeader.classList.add("card-header");
          cardHeader.setAttribute("id", "heading" + accordionId);

          // Create button for header
          var button = document.createElement("button");
          button.classList.add("btn", "btn-link");
          button.setAttribute("data-bs-toggle", "collapse");
          button.setAttribute("data-bs-target", "#collapse" + accordionId);
          button.setAttribute("aria-expanded", "true"); // Set to true to open accordion by default
          button.setAttribute("aria-controls", "collapse" + accordionId);
          button.innerHTML = "Accordion " + (index + 1);

          // Append button to header
          cardHeader.appendChild(button);

          // Append header to card
          accordionCard.appendChild(cardHeader);

          // Create accordion body
          var accordionBody = document.createElement("div");
          accordionBody.classList.add("collapse", "show"); // Add the "show" class to make it visible by default
          accordionBody.setAttribute("id", "collapse" + accordionId);
          accordionBody.setAttribute("aria-labelledby", "heading" + accordionId);
          accordionBody.setAttribute("data-bs-parent", "#accordionContainer");

          // Create card body content
          var cardBody = document.createElement("div");
          cardBody.classList.add("card-body");
          cardBody.innerHTML = item;

          // Append body content to accordion body
          accordionBody.appendChild(cardBody);

          // Append accordion body to card
          accordionCard.appendChild(accordionBody);

          // Append card to accordion container
          accordionContainer.appendChild(accordionCard);
        });
      });
    </script>
  </body>
</html>
















<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    <div class="container" id="dataContainer">
      <!-- Content will be added here dynamically -->
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <script>
      // Fetch data from the URL
      fetch('https://bimbelpro.com/explain-answer?user_id=82a69229-f9bb-44df-9258-f97b8bac242d&package_id=41076d1c-0937-49e0-832f-9813c5abed32')
        .then(response => response.json())
        .then(data => {
            console.log(data);
          // Display data in the container
          document.getElementById('dataContainer').innerHTML = JSON.stringify(data, null, 2);
        })
        .catch(error => console.error('Error fetching data:', error));
    </script>
  </body>
</html>
