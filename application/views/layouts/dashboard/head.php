<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>TFITNESS | <?= $title ?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link rel="icon" href="<?= base_url('assets/img/logo.png') ?>">
	<!-- ================== BEGIN core-css ================== -->
	<link href="<?= base_url('assets/dashboard/css/vendor.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/css/facebook/app.min.css') ?>" rel="stylesheet" />
	<!-- ================== END core-css ================== -->

	<link href="<?= base_url('assets/dashboard/plugins/select2/dist/css/select2.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') ?>" rel="stylesheet" />
</head>

<body>