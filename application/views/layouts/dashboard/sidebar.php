<div id="sidebar" class="app-sidebar" style="background-color: azure;">
	<!-- BEGIN scrollbar -->
	<div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
		<!-- BEGIN menu -->
		<div class="menu">
			<div class="menu-profile">
				<a href="javascript:;" class="menu-profile-link" data-toggle="app-sidebar-profile" data-target="#appSidebarProfileMenu">
					<div class="menu-profile-cover with-shadow"></div>
					<div class="menu-profile-image">
						<img src="<?= base_url('/assets/img/uploads/' . $this->session->userdata('photo')) ?>" alt="" />
					</div>
					<div class="menu-profile-info">
						<div class="d-flex align-items-center">
							<div class="flex-grow-1">
								<?= $this->session->userdata('name') ?>
							</div>
							<div class="menu-caret ms-auto"></div>
						</div>
						<small><?= $this->session->userdata('role') ?></small>
					</div>
				</a>
			</div>
			<div id="appSidebarProfileMenu" class="collapse">
				<div class="menu-item pt-5px">
					<a href="<?= base_url('dashboard/setting') ?>" class="menu-link">
						<div class="menu-icon"><i class="fa fa-cog"></i></div>
						<div class="menu-text">Pengaturan</div>
					</a>
				</div>
				<div class="menu-item pt-5px">
					<a href="<?= base_url('auth/logout') ?>" class="menu-link">
						<div class="menu-icon"><i class="fa fa-sign-out-alt"></i></div>
						<div class="menu-text">Logout</div>
					</a>
				</div>
				<div class="menu-divider m-0"></div>
			</div>
			<div class="menu-header">Navigation</div>
			<div class="menu-item <?= ($title == 'Devices') ? 'active' : '' ?>">
				<a href="<?= base_url('/dashboard/device') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fa fa-tablet"></i>
					</div>
					<div class="menu-text">Device</div>
				</a>
			</div>
			<?php if ($this->session->userdata('role') == 'admin') : ?>
				<div class="menu-item <?= ($title == 'Admins') ? 'active' : '' ?>">
					<a href="<?= base_url('/dashboard/admin') ?>" class="menu-link">
						<div class="menu-icon">
							<i class="fa fa-user-circle"></i>
						</div>
						<div class="menu-text">User</div>
					</a>
				</div>
				<div class="menu-item <?= ($title == 'Log Report') ? 'active' : '' ?>">
					<a href="<?= base_url('/dashboard/log_report') ?>" class="menu-link">
						<div class="menu-icon">
							<i class="fa fa-bars"></i>
						</div>
						<div class="menu-text">Log Report</div>
					</a>
				</div>
			<?php endif ?>
			<div class="menu-item <?= ($title == 'Members') ? 'active' : '' ?>">
				<a href="<?= base_url('/dashboard/member') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fa fa-users"></i>
					</div>
					<div class="menu-text">Member</div>
				</a>
			</div>
			<div class="menu-item <?= ($title == 'Members Search') ? 'active' : '' ?>">
				<a href="<?= base_url('/dashboard/member_search') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fa fa-users"></i>
					</div>
					<div class="menu-text">Member Search</div>
				</a>
			</div>
			<!-- <div class="menu-item <?= ($title == 'Member Images') ? 'active' : '' ?>">
				<a href="<?= base_url('/dashboard/memberimage') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fa fa-users"></i>
					</div>
					<div class="menu-text">Member Image</div>
				</a>
			</div> -->



			<!-- BEGIN minify-button -->
			<div class="menu-item d-flex">
				<a href="javascript:;" class="app-sidebar-minify-btn ms-auto" data-toggle="app-sidebar-minify"><i class="fa fa-angle-double-left"></i></a>
			</div>
			<!-- END minify-button -->
		</div>
		<!-- END menu -->
	</div>
	<!-- END scrollbar -->
</div>
<div class="app-sidebar-bg"></div>
<div class="app-sidebar-mobile-backdrop"><a href="#" data-dismiss="app-sidebar-mobile" class="stretched-link"></a></div>
<!-- END #sidebar -->
