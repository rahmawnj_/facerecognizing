<!DOCTYPE html>
<html>

<head>
	<title>Log Image</title>
	<link rel="icon" href="<?= base_url('assets/img/logo.png') ?>">
	<link href="<?= base_url('assets/landingpage/bootstrap-5.3.0-alpha1-dist/css/bootstrap.min.css') ?>" rel="stylesheet" />

	<style>
		.card {
			flex-direction: row;
			align-items: center;
		}

		#message {
			text-align: center;
			/* rata tengah*/
			line-height: 400px;
			/* sesuaikan dengan tinggi card */
		}

		.card-title {
			font-weight: bold;
		}

		.card img {
			width: 30%;
			border-top-right-radius: 0;
			border-bottom-left-radius: calc(0.25rem - 1px);
		}

		@media only screen and (max-width: 768px) {
			a {
				display: none;
			}

			.card-body {
				padding: 0.5em 1.2em;
			}

			.card-body .card-text {
				margin: 0;
			}

			.card img {
				width: 50%;
			}
		}

		@media only screen and (max-width: 1200px) {
			.card img {
				width: 40%;
			}
		}

		.indicator {
			position: absolute;
			top: 0px;
			right: 0px;
			display: flex;
			align-items: center;
		}

		.status-circle {
			width: 20px;
			height: 20px;
			border-radius: 10px;
			margin-right: 5px;
		}

		.status {
			font-size: 18px;
			font-weight: bold;
		}
	</style>
</head>

<body style="background-color:midnightblue; margin:80px; overflow: hidden;">
	<div class="container">
		<div class="card" style="width: 100%; height: 80vh;">
			<div class="indicator" style="margin: 5px;" class="m-3">
				<div id="status-circle" class="status-circle"></div>
				<span id="status" class="status"></span>
			</div>
			<img id="image" style="display: none; height:100%;" class="card-img-top" alt="...">
			<div class="card-body">
				<h1 id="message" class="text-center">TFITNESS</h1>

				<h1 id="res-title" class="card-text"></h1>
				<h2 id="response-text" class="card-title"></h2>
				<h2 id="res-ket" class="card-text"></h2>
			</div>
		</div>
		<audio id="expireAudioPlayer" controls style="display: none">
			<source src="<?= base_url('/assets/music/uploads/setting/' . $this->db->get_where('setting', ['key' => 'expire_sound'])->row_array()['value']) ?>" type="audio/mpeg">
			Your browser does not support the audio element.
		</audio>
		<audio id="activeAudioPlayer" controls style="display: none">
			<source src="<?= base_url('/assets/music/uploads/setting/' . $this->db->get_where('setting', ['key' => 'active_sound'])->row_array()['value']) ?>" type="audio/mpeg">
			Your browser does not support the audio element.
		</audio>
		<audio id="unrecognizeAudioPlayer" controls style="display: none">
			<source src="<?= base_url('/assets/music/uploads/setting/' . $this->db->get_where('setting', ['key' => 'unrecognize_sound'])->row_array()['value']) ?>" type="audio/mpeg">
			Your browser does not support the audio element.
		</audio>
	</div>

	<script src="<?= base_url('assets/landingpage/bootstrap-5.3.0-alpha1-dist/js/bootstrap.bundle.min.js') ?>" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
	<script>

		let lastImageUrl = '';
		let intervalId;
		let timeoutId;
		let isImageShown = false;
		let time = 0;
		let count = 0;
		let musicPlayed = false; // Add this variable


		document.addEventListener("DOMContentLoaded", function() {
			intervalId = setInterval(fetchData, 2000);
		});
		
let previousLastUpdateTime = 0;
	var previousLogTime = null;
	function fetchData() {
    let nowTime = Math.floor(Date.now() / 1000)
    count++;

    if (count % 10000 === 0) {
        location.reload();
    }
	

	
    fetch('<?= base_url('api/last_log/' . $id) ?>')
        .then(response => response.json())
        .then(data => {

			lastUpdateTime = data.logs.time;

if (previousLastUpdateTime !== lastUpdateTime) {
	previousLastUpdateTime = lastUpdateTime;

	// Periksa apakah jenis musik yang akan diputar sama dengan yang terakhir kali diputar
	if (audioPlayedType !== '') {
		// Hentikan pemutaran jika jenis musik sama
		if (audioPlayedType === 'expire' && audioPlayer === document.getElementById("expireAudioPlayer")) {
			return;
		} else if (audioPlayedType === 'active' && audioPlayer === document.getElementById("activeAudioPlayer")) {
			return;
		} else if (audioPlayedType === 'unrecognize' && audioPlayer === document.getElementById("unrecognizeAudioPlayer")) {
			return;
		}
	}

	console.log('nyala');
	playMusic();
	console.log(musicPlayed);
	musicPlayed = true;
}
            if (nowTime - data.logs.time_heartbeat > 10) {
                document.getElementById("status").innerHTML = "Offline";
                document.getElementById("status-circle").style.backgroundColor = 'red';
            } else {
                document.getElementById("status").innerHTML = "Online";
                document.getElementById("status-circle").style.backgroundColor = 'green';
            }

            if (nowTime - data.logs.time < 20) {
                if (data.logs.image) {
                    // Simpan URL gambar yang didapat dari API
                    lastImageUrl = data.logs.image;
                    // Tampilkan gambar yang didapat dari API
                    document.getElementById("message").innerHTML = '';
                    document.getElementById("image").src = data.logs.image;
                    document.getElementById("image").style.display = 'block';
                    document.getElementById("image").style.height = '80%';
                    document.getElementById("image").style.width = '25%';
                    document.getElementById("image").style.marginLeft = '10%';
                    document.getElementById("image").style.marginBottom = '5%';
                    isImageShown = true;
                    // Tampilkan response text
                    if (data.logs.operator === 'VerifyPush') {
                        let checkExpire = '';
                        document.getElementById("res-title").style.marginBottom = '10px';
                        document.getElementById("res-title").style.textAlign = 'center';
                        document.getElementById("response-text").innerHTML = 'NAMA : ' + data.data_user.nama;
                        document.getElementById("response-text").style.textAlign = 'center';
                        if (data.data_user.jatuhtempo) {
                            document.getElementById("res-ket").innerHTML = 'MASA AKTIV : ' + data.data_user.jatuhtempo;
                            document.getElementById("res-ket").style.textAlign = 'center';
                        }

						if (new Date() >= new Date(data.data_user.jatuhtempo)) {
    checkExpire = 'Status Member : Kadaluarsa';
    playMusic('expire'); // Play expire sound
} else {
    checkExpire = 'Status Member : Aktif';
    playMusic('active'); // Play active sound
}


                    } else {
                        document.getElementById("response-text").innerHTML = 'TIDAK TERDAFTAR';
                        document.getElementById("res-ket").innerHTML = '';
                        document.getElementById("res-ket").style.textAlign = 'center';
                        document.getElementById("response-text").style.textAlign = 'center';
                        document.getElementById("res-title").style.textAlign = 'center';
                        // Unrecognize sound
						playMusic('unrecognize'); // Play unrecognize sound

                    }
                    clearTimeout(timeoutId);
                    timeoutId = setTimeout(hideImage, 2000);
                    time = 0;

                }
            }

        });
}

function playMusic(type) {
        let audioPlayer;

        switch (type) {
            case 'expire':
                audioPlayer = document.getElementById("expireAudioPlayer");
                break;
            case 'active':
                audioPlayer = document.getElementById("activeAudioPlayer");
                break;
            case 'unrecognize':
                audioPlayer = document.getElementById("unrecognizeAudioPlayer");
                break;
            default:
                return;
        }

        // Hentikan audio yang sedang dimainkan
        document.querySelectorAll('audio').forEach(function(audio) {
            if (audio !== audioPlayer && !audio.paused) {
                audio.pause();
                audio.currentTime = 0; // Set currentTime ke 0 agar audio bisa diputar lagi
            }
        });

        // Mulai audio yang dipilih
        audioPlayer.addEventListener('ended', function() {
            audioPlayer.pause();
            audioPlayer.currentTime = 0;
            console.log('end');
        });

        if (audioPlayer) {
            audioPlayer.play();
            audioPlayedType = type; // Tetapkan audioPlayedType setelah memulai pemutaran
        }
    }

		function hideImage() {

			document.getElementById("image").style.display = 'none';
			isImageShown = false;
			document.getElementById("message").innerHTML = 'TFITNESS';
			document.getElementById("res-ket").innerHTML = '';
			document.getElementById("response-text").innerHTML = '';
			document.getElementById("res-title").innerHTML = '';
		}
	</script>
</body>

</html>