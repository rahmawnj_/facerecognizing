<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
	<!-- END #header -->
	<?php $this->load->view('layouts/dashboard/headbar') ?>
	<!-- BEGIN #sidebar -->
	<?php $this->load->view('layouts/dashboard/sidebar') ?>

	<div id="content" class="app-content">
		<!-- BEGIN breadcrumb -->
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
			<li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
		</ol>
		<!-- END breadcrumb -->
		<!-- BEGIN page-header -->
		<!-- END page-header -->
		<!-- BEGIN row -->
		<div class="row">
			<!-- BEGIN col-2 -->

			<!-- END col-2 -->
			<!-- BEGIN col-10 -->
			<div class="col-xl-12">
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<!-- BEGIN panel-heading -->
					<div class="panel-heading">
						<h4 class="panel-title"><?= $title ?></h4>
						<div class="panel-heading-btn">

						</div>
					</div>

					<?= $this->session->flashdata('message'); ?>

					<div class="panel-body">
						<?php echo form_open_multipart('dashboard/setting_update'); ?>
						<fieldset>
							<?= $this->session->flashdata('message'); ?>
							<div class="mb-3">
								<audio controls>
									<source src="<?= base_url('/assets/music/uploads/setting/' . $this->db->get_where('setting', ['key' => 'expire_sound'])->row_array()['value']) ?>" type="audio/mpeg">
									Your browser does not support the audio element.
								</audio>
							</div>

							<div class="mb-3">
								<div class="form-group">
									<label for="expire_sound">Expire Sound</label>
									<input type="file" class="form-control" name="expire_sound" id="expire_sound" aria-describedby="expire_sound" placeholder="expire_Sound">
									<span class="text-danger">
										<?= form_error('expire_sound') ?>
									</span>
								</div>
							</div>
							<div class="mb-3">
								<audio controls once  >
									<source src="<?= base_url('/assets/music/uploads/setting/' . $this->db->get_where('setting', ['key' => 'active_sound'])->row_array()['value']) ?>" type="audio/mpeg">
									Your browser does not support the audio element.
								</audio>
							</div>

							<div class="mb-3">
								<div class="form-group">
									<label for="active_sound">Active Sound</label>
									<input type="file" class="form-control" name="active_sound" id="active_sound" aria-describedby="active_sound" placeholder="active_Sound">
									<span class="text-danger">
										<?= form_error('active_sound') ?>
									</span>
								</div>
							</div>
							<div class="mb-3">
								<audio controls once  >
									<source src="<?= base_url('/assets/music/uploads/setting/' . $this->db->get_where('setting', ['key' => 'unrecognize_sound'])->row_array()['value']) ?>" type="audio/mpeg">
									Your browser does not support the audio element.
								</audio>
							</div>

							<div class="mb-3">
								<div class="form-group">
									<label for="unrecognize_sound">Unrecognize Sound</label>
									<input type="file" class="form-control" name="unrecognize_sound" id="unrecognize_sound" aria-describedby="unrecognize_sound" placeholder="unrecognize_Sound">
									<span class="text-danger">
										<?= form_error('unrecognize_sound') ?>
									</span>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
							</div>
						</fieldset>
						<?= form_close() ?>
					</div>
				</div>
				<!-- END panel -->
			</div>
			<!-- END col-10 -->
		</div>
		<!-- END row -->
	</div>
	<!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>
