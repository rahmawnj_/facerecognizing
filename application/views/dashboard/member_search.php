<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                        </div>
                    </div>

                    <?= $this->session->flashdata('message'); ?>
                    <div class="panel-body">
                        <form action="<?= base_url('dashboard/member_search') ?>" method="get">
                            <?= $this->session->flashdata('message'); ?>
                            <div class="input-group mb-3">
                                <select class="form-select" name="id_device" id="id_device">
                                    <option selected disabled>-- Device --</option>
                                    <?php foreach ($devices as $device) : ?>
                                        <option value="<?= $device['id'] ?>" <?= ($device['id'] == $this->input->get('id_device') ? 'selected' : '') ?>><?= $device['nama'] ?></option>
                                    <?php endforeach ?>
                                </select>
                                <select name="id_member" class="form-select option-name" id="member">
                                    <option value="">-- Nama --</option>
                                    <?php foreach ($members as $member) : ?>
                                        <option value="<?= $member['id_member'] ?>" <?= ($member['id_member'] == $this->input->get('id_member') ? 'selected' : '') ?>><?= $member['nama'] ?> (<?= $member['memberid'] ?>)</option>
                                    <?php endforeach ?>
                                </select>
                                <button type="submit" class="btn btn-primary">Cari</button>
                            </div>
                        </form>

                        <?php if (isset($dataSearch['info']['List'])) : ?>
                            <div class="container">
                                <h1><?= $dataSearch['info']['List'][0]['Name']; ?></h1>
                                <img class="img-fluid" src="<?= $dataSearch['info']['List'][0]['Picinfo']; ?>" alt="">
                                <div class="d-inline">
                                    <a href="<?= base_url('dashboard/member_search_delete?CustomizeID=' . $dataSearch['info']['List'][0]['CustomizeID'] . '&DeviceID=' . $dataSearch['info']['DeviceID']) ?>" class="btn btn-danger">Hapus</a>
                                    <form action="<?= base_url('dashboard/member_search_update') ?>" method="post" class="d-inline">
                                        <input type="hidden" name="uuid" value="<?= $dataSearch['info']['List'][0]['CustomizeID'] ?>">
                                        <input type="hidden" name="id_member" value="<?= $this->input->get('id_member') ?>">
                                        <input type="hidden" name="deviceid" value="<?= $dataSearch['info']['DeviceID'] ?>">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </form>
                                </div>
                            </div>
                        <?php endif ?>


                    </div>
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>