<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">

                            <a href="<?= base_url('/dashboard/member_create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                        </div>
                    </div>


                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>


                    <div class="panel-body" class="table-responsive">
                        <form method="post" action="<?= base_url('dashboard/export_member') ?>">
                            <div class="input-group mb-3">
                                <select class="form-control" id="inline-form-name" name="id_device" id="">
                                    <option selected disabled>-- Device Master --</option>
                                    <?php foreach ($devices as $device) : ?>
                                        <option value="<?= $device['id'] ?>" <?= ($device['master']) ? 'selected' : '' ?>><?= $device['nama'] ?></option>
                                    <?php endforeach ?>
                                </select>
                                <button type="submit" class="btn btn-primary">Export</button>
                            </div>
                            <a href="<?= base_url('dashboard/download_export') ?>" class="btn btn-success">Download File</a>

                            <table style="width: 100%;" id="data-table-default" class="table table-striped table-bordered align-middle">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th><input type="checkbox" onchange="checkAll(this)" name="chk[]">
                                        <th>Photo</th>
                                        <th>Nama</th>
                                        <th>Member ID</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0;
                                    foreach ($members as $member) : ?>
                                        <tr>
                                            <td><?= ++$no; ?></td>
                                            <td><input type="checkbox" name="memberid[]" value="<?= $member['memberid'] ?>" id=""></td>
                                            <td>
                                                <img src="<?= base_url('/assets/img/uploads/' . $member['photo']) ?>" class="img-fluid img-thumbnail mx-auto d-block text-center overflow-hidden" style="height:100px; width:100px" alt="">
                                            </td>
                                            <td><?= $member['nama'] ?></td>
                                            <td><?= $member['memberid'] ?></td>
                                            <td>
                                                <?php if ($this->session->userdata('role') == 'admin') : ?>
                                                    <a class="fa btn-sm fa-edit btn bg-warning text-white" href="<?= base_url('dashboard/member_edit/' . $member['id_member']) ?>"></a>
                                                    <a id="delete-button" class="fas btn-sm fa-trash btn bg-danger text-white" href="<?= base_url('dashboard/member_delete/' . $member['id_member']) ?>"></a>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('layouts/dashboard/foot') ?>

<script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox' && !(checkboxes[i].disabled)) {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }
</script>