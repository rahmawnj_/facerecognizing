<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>

        <div class="row">

            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">

                            <!-- <a href="<?= base_url('/dashboard/member_create') ?>" class="btn btn-primary float-right fas fa-plus"></a> -->
                        </div>
                    </div>

                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

                    <div class="panel-body table-responsive" class="">
                        <div class="input-group mb-3">
                            <a href="<?= base_url('dashboard/download_images') ?>" class="btn btn-success">Download Images</a>
                        </div>

                        <table style="width: 100%;" id="data-table-export" class="table table-striped table-bordered align-middle">
                            <thead>
                                <tr>
                                    <th>List type</th>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Dept.</th>
                                    <th>ID no.</th>
                                    <th>Phone no.</th>
                                    <th>Address</th>
                                    <th>Date of birth</th>
                                    <th>Card no. generation method</th>
                                    <th>Card no.</th>
                                    <th>Built-in card no.</th>
                                    <th>Custom ID</th>
                                    <th>Permanent list</th>
                                    <th>Start time</th>
                                    <th>End time</th>
                                    <th>Number of valid temporary list 3</th>
                                    <th>Entry date</th>
                                    <th>Picture path</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;
                                foreach ($members as $member) : ?>
                                    <tr>
                                        <td>Whitelist</td>
                                        <td><?= $member['nama'] ?></td>
                                        <td></td>
                                        <td><?= $member['id_member']?></td>
                                        <td><?= $member['id_member']?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><?= $member['id_member']?></td>
                                        <td><?= $member['id_member']?></td>
                                        <td><?= $member['id_member']?></td>
                                        <td><?= $member['id_member']?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><?= explode('/', $member['photo'])[1] ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('layouts/dashboard/foot') ?>

<script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox' && !(checkboxes[i].disabled)) {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }
</script>