<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                            <a href="<?= base_url('/dashboard/memberimage_create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                        </div>
                    </div>


                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>


                    <div class="panel-body" class="table-responsive">
                        <table style="width: 100%;" id="data-table-default" class="table table-striped table-bordered align-middle">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;
                                foreach ($memberImages as $member) : ?>
                                    <tr>
                                        <td><?= ++$no; ?></td>
                                        <td>
                                            <img src="<?= $member['Picinfo'] ?>" class="img-fluid img-thumbnail mx-auto d-block text-center overflow-hidden" style="height:100px; width:100px" alt="">
                                        </td>
                                        <td><?= $member['Name'] ?></td>
                                        <td>
                                            <?php if ($this->session->userdata('role') == 'admin') : ?>
                                                <a class="fa btn-sm fa-edit btn bg-warning text-white" href="<?= base_url('dashboard/memberimage_edit/' . $member['CustomizeID'] . '/' . $member['Name']) ?>"></a>
                                                <a id="delete-button" class="fas btn-sm fa-trash btn bg-danger text-white" href="<?= base_url('dashboard/memberimage_delete/' . $member['CustomizeID']) ?>"></a>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- END hljs-wrapper -->
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>