<?php $this->load->view('layouts/dashboard/head') ?>
<style type="text/css">
    video {
        width: 100%;
        max-width: 200px;
        height: auto;
        display: block;
        margin: 0 auto;
    }
</style>
<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                    </div>

                    <?= $this->session->flashdata('message'); ?>

                    <div class="panel-body">
                        <?php echo form_open_multipart('dashboard/memberimage_store'); ?>

                        <?= $this->session->flashdata('message'); ?>
                        <fieldset>
                            <input type="hidden" name="foto_webcam" value="">

                            <div class="mb-3">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="pilihan" value="webcam" id="webcam">
                                        <label class="form-check-label">
                                            Gunakan Webcam
                                        </label>

                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="pilihan" value="upload" id="upload">
                                        <label class="form-check-label">
                                            Upload dari Komputer
                                        </label>
                                    </div>
                                    <div class="img-preview d-flex">
                                    <img src="<?= base_url('assets/img/default_user.jfif') ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:50px" alt="">
                                </div>
                                    <div id="input_foto"></div><br>
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="id_member">Nama</label>
                                    <select name="id_member" class="option-name form-control">
                                        <optgroup label="">
                                            <option disabled selected>-- Nama --</option>
                                            <?php foreach ($members as $key => $member) : ?>
                                                <option value="<?= $member['id'] ?>"><?= $member['nama'] ?></option>
                                            <?php endforeach ?>
                                        </optgroup>
                                    </select>
                                    <span class="text-danger">
                                        <?= form_error('id_member') ?>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Daftar" name="submit" class="btn btn-primary w-100px me-5px">
                            </div>
                        </fieldset>
                        <?= form_close() ?>

                    </div>
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>

<script>
    var radioWebcam = document.getElementById("webcam");
    var radioUpload = document.getElementById("upload");
    const inputFoto = document.getElementById("input_foto");
    const previewFoto = document.getElementById("gmbr");


    radioWebcam.addEventListener("click", function() {
        inputFoto.innerHTML = '<video id="video" autoplay></video><br><a id="capture" class="btn btn-success btn-sm" >Ambil Foto</a><canvas id="canvas"></canvas>';
        var video = document.getElementById("video");
        var canvas = document.getElementById("canvas");
        var ctx = canvas.getContext('2d');
        navigator.mediaDevices.getUserMedia({
                video: true
            })
            .then(function(stream) {
                video.srcObject = stream;
            })
            .catch(function(error) {
                console.log("Error:", error);
            });
        var captureButton = document.getElementById("capture");
        captureButton.addEventListener("click", function() {
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
        });
    });

    radioUpload.addEventListener("click", function() {
        inputFoto.innerHTML = '<input type="file" class="form-control" name="foto" id="foto">';
    });


    inputFoto.addEventListener("change", function(event) {
        const file = event.target.files[0];
        const reader = new FileReader();

        reader.addEventListener("load", function() {
            previewFoto.src = reader.result;
        });

        if (file) {
            reader.readAsDataURL(file);
        }
    });

    document.querySelector("form").addEventListener("submit", function(event) {
        if (document.getElementById("webcam").checked) {
            var canvas = document.getElementById("canvas");
            var gambar = canvas.toDataURL();
            document.getElementsByName("foto_webcam")[0].value = gambar;
        }
    });
</script>