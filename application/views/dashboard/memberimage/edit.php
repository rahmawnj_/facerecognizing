<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                             </div>
                    </div>

                    <?= $this->session->flashdata('message'); ?>

                    <div class="panel-body">

                        <?php echo form_open_multipart('dashboard/memberimage_update'); ?>
                        <div class="img-preview d-flex">
                            <img src="<?= $memberImage['Picinfo'] ?>" id="gmbr" class="img-fluid img-thumbnail d-block" style="height:100px;" alt="">
                        </div>
                        <h2><?= $memberImage['Name'] ?></h2>
                        <br>
                        <fieldset>
                            <?= $this->session->flashdata('message'); ?>
                            <input type="hidden" name="PersonUUID" value="<?= $memberImage['PersonUUID'] ?>">
                            <input type="hidden" name="CustomizeID" value="<?= $memberImage['CustomizeID'] ?>">
                            <input type="hidden" name="foto_webcam" value="">
                            <input type="hidden" name="id_member" value="<?= $memberImage['Name'] ?>">

                            <div class="mb-3">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="pilihan" value="webcam" id="webcam">
                                        <label class="form-check-label">
                                            Gunakan Webcam
                                        </label>

                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="pilihan" value="upload" id="upload">
                                        <label class="form-check-label">
                                            Upload dari Komputer
                                        </label>
                                    </div>
                                    <br>
                                    <div id="input_foto"></div><br>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                            </div>
                        </fieldset>
                        <?= form_close() ?>
                    </div>
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>
<script>
    var radioWebcam = document.getElementById("webcam");
    var radioUpload = document.getElementById("upload");
    const inputFoto = document.getElementById("input_foto");
    const previewFoto = document.getElementById("gmbr");


    radioWebcam.addEventListener("click", function() {
        inputFoto.innerHTML = '<video id="video" autoplay></video><br><a id="capture" class="btn btn-success btn-sm" >Ambil Foto</a> <br><canvas style="height:100px;" id="canvas"></canvas>';
        var video = document.getElementById("video");
        var canvas = document.getElementById("canvas");

        var ctx = canvas.getContext('2d');
        navigator.mediaDevices.getUserMedia({
                video: true
            })
            .then(function(stream) {
                video.srcObject = stream;
            })
            .catch(function(error) {
                console.log("Error:", error);
            });
        var captureButton = document.getElementById("capture");
        captureButton.addEventListener("click", function() {
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
        });
    });

    radioUpload.addEventListener("click", function() {
        inputFoto.innerHTML = '<input type="file" class="form-control" name="foto" id="foto">';
    });


    inputFoto.addEventListener("change", function(event) {
        const file = event.target.files[0];
        const reader = new FileReader();

        reader.addEventListener("load", function() {
            previewFoto.src = reader.result;
        });

        if (file) {
            reader.readAsDataURL(file);
        }
    });

    document.querySelector("form").addEventListener("submit", function(event) {
        if (document.getElementById("webcam").checked) {
            var canvas = document.getElementById("canvas");
            var gambar = canvas.toDataURL();
            document.getElementsByName("foto_webcam")[0].value = gambar;
        }
    });
</script>