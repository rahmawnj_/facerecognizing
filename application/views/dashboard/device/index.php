<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                            <?php if ($this->session->userdata('role') == 'admin') : ?>
                                <a href="<?= base_url('/dashboard/device_create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>
                    <!-- <div class="flash-data-success-refresh" data-flashdatasuccessrefresh="<?= $this->session->flashdata('error') ?>"></div> -->

                    <div class="panel-body" class="table-responsive">
                        <!-- <?php if ($this->session->userdata('role') == 'admin') : ?>
                            <form method="post" action="<?= base_url('dashboard/device_update_master') ?>" class="row row-cols-lg-auto my-3">
                                <div class="col">
                                    <label class="visually-hidden" for="inline-form-name">Name</label>
                                    <select class="form-control" id="inline-form-name" name="id_device" id="">
                                        <option selected disabled>-- Device Master --</option>
                                        <?php foreach ($devices as $device) : ?>
                                            <option value="<?= $device['id'] ?>" <?= ($device['master']) ? 'selected' : '' ?>><?= $device['nama'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="col">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
                        <?php endif ?> -->

                        <table style="width: 100%;" id="data-table-default" class="table table-striped table-bordered align-middle">
                            <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th>Device ID</th>
                                    <th>Name</th>
                                    <th>Gate</th>
                                    <th>IP Address</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;
                                foreach ($devices as $device) : ?>
                                    <tr>
                                        <td><?= ++$no; ?></td>
                                        <td><?= $device['deviceid'] ?></td>
                                        <td><?= $device['nama'] ?></td>
                                        <td><?= $device['gate'] ?></td>
                                        <td><?= $device['ip_address'] ?></td>
                                        <td>
                                            <?php if ($this->session->userdata('role') == 'admin') : ?>
                                                <a class="fa btn-sm fa-edit btn bg-warning text-white" href="<?= base_url('dashboard/device_edit/' . $device['id']) ?>" data-toggle="tooltip" data-placement="top" title="Edit Device"></a>
                                                <a class="fa btn-sm fa-search btn bg-info text-white" href="<?= base_url('welcome/index/' . $device['deviceid']) ?>" data-toggle="tooltip" data-placement="top" title="Go To Welcome Page Device"></a>
                                            <?php endif ?>

                                            <a class="fa btn-sm fa-trash btn bg-indigo text-white" href="<?= base_url('dashboard/emptyImage/' . $device['deviceid']) ?>" data-toggle="tooltip" data-placement="top" title="Delete Images"></a>
                                            <!-- <a class="fa btn-sm fa-upload btn bg-teal text-white" href="<?= base_url('dashboard/export_images/' . $device['deviceid']) ?>" data-toggle="tooltip" data-placement="top" title="Clone Images"></a> -->
                                            <a id="delete-button" class="fas btn-sm fa-times-circle btn bg-danger text-white" href="<?= base_url('dashboard/device_delete/' . $device['id']) ?>" data-toggle="tooltip" data-placement="top" title="Delete Device"></a>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- END hljs-wrapper -->
                </div>
                <!-- END panel -->
            </div>]
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>