<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                        </div>
                    </div>
                   
                    <?= $this->session->flashdata('message'); ?>

                    <div class="panel-body">
                        <?php echo form_open_multipart('dashboard/device_store'); ?>
                            <?= $this->session->flashdata('message'); ?>
                            <fieldset>
                
                                <div class="mb-3">
                                    <div class="form-group">
                                        <label for="nama">Nama</label>
                                        <input type="text" name="nama" class="form-control">
                                        <span class="text-danger">
                                            <?= form_error('nama') ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-group">
                                        <label for="deviceid">Device ID</label>
                                        <input type="text" name="deviceid" class="form-control">
                                        <span class="text-danger">
                                            <?= form_error('deviceid') ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-group">
                                        <label for="ip_address">Ip Address</label>
                                        <input type="text" name="ip_address" class="form-control">
                                        <span class="text-danger">
                                            <?= form_error('ip_address') ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-group">
                                        <label for="gate">Gate</label>
                                        <select class="form-control" name="gate" id="gate">
                                            <option value="" selected disabled>Gate</option>
                                            <option value="in">In</option>
                                            <option value="out">Out</option>
                                        </select>
                                        <span class="text-danger">
                                            <?= form_error('gate') ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                                </div>
                            </fieldset>
                        <?= form_close() ?>
                    </div>
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>