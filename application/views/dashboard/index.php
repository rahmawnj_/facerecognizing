<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <?php if ($this->session->userdata('role') == 'admin') : ?>

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- END breadcrumb -->
            <!-- BEGIN page-header -->
            <h1 class="page-header">Dashboard</h1>
            <!-- END page-header -->

            <!-- BEGIN row -->
            <div class="row">
                <!-- BEGIN col-3 -->
                <div class="col-xl-3 col-md-6">
                    <div class="widget widget-stats bg-blue">
                        <div class="stats-icon"><i class="fa fa-angle-double-right"></i></div>
                        <div class="stats-info">
                            <h4>RECOGNITION - IN</h4>
                            <p><?= $in_recognition ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="widget widget-stats bg-info">
                        <div class="stats-icon"><i class="fa fa-angle-double-left"></i></div>
                        <div class="stats-info">
                            <h4>RECOGNITION - OUT</h4>
                            <p><?= $out_recognition ?></p>
                        </div>

                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="widget widget-stats bg-orange">
                        <div class="stats-icon"><i class="fa fa-angle-double-right"></i></div>
                        <div class="stats-info">
                            <h4>RFID - IN</h4>
                            <p><?= $in_rfid ?></p>
                        </div>

                    </div>
                </div>
                <!-- END col-3 -->
                <!-- BEGIN col-3 -->
                <div class="col-xl-3 col-md-6">
                    <div class="widget widget-stats bg-red">
                        <div class="stats-icon"><i class="fa fa-angle-double-left"></i></div>
                        <div class="stats-info">
                            <h4>RFID - OUT</h4>
                            <p><?= $out_rfid ?></p>
                        </div>

                    </div>
                </div>
                <!-- END col-3 -->
            </div>
            <!-- END row -->
        <?php endif ?>
        <!-- BEGIN row -->
        <div class="row">
            <div class="col-12 d-flex justify-content-center mt-5">
                <img src="<?= base_url('assets/img/logo-apk.png') ?>" alt="" style="width:70%;" srcset="">
            </div>
        </div>
        <!-- END row -->
    </div>

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.canvaswrapper.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.colorhelpers.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.saturated.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.browser.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.drawSeries.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.uiConstants.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.time.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.resize.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.pie.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.crosshair.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.categories.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.navigate.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.touchNavigate.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.hover.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.touch.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.selection.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.symbol.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/flot/source/jquery.flot.legend.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/jquery-sparkline/jquery.sparkline.min.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/jvectormap-next/jquery-jvectormap.min.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/jvectormap-next/jquery-jvectormap-world-mill.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') ?>"></script>