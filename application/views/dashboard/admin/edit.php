<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                           
                        </div>
                    </div>

                    <?= $this->session->flashdata('message'); ?>

                    <div class="panel-body">
                        <?php echo form_open_multipart('dashboard/admin_update'); ?>
                        <div class="img-preview d-flex">
                            <img src="<?= base_url('/assets/img/uploads/' . $admin['photo']) ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:50px;" alt="">
                        </div>
                        <fieldset>
                            <?= $this->session->flashdata('message'); ?>
                            <input type="hidden" name="id_admin_face" value="<?= $admin['id_admin_face'] ?>">

                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="photo">Photo</label>
                                    <input type="file" id="gambar" class="form-control" size="20" name="photo" aria-describedby="photo" placeholder="photo">
                                    <span class="text-danger">
                                        <?= form_error('photo') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input autocomplete="off" type="text" class="form-control" name="name" value="<?= $admin['name'] ?>" id="name" aria-describedby="name" placeholder="name">
                                    <span class="text-danger">
                                        <?= form_error('name') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input autocomplete="off" type="text" class="form-control" value="<?= $admin['username'] ?>" name="username" id="username" aria-describedby="username" placeholder="Username">
                                    <span class="text-danger">
                                        <?= form_error('username') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="email">email</label>
                                    <input autocomplete="off" type="text" class="form-control" value="<?= $admin['email'] ?>" name="email" id="email" aria-describedby="email" placeholder="email">
                                    <span class="text-danger">
                                        <?= form_error('email') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input autocomplete="off" type="password" class="form-control" name="password" id="password" aria-describedby="password" placeholder="Password">
                                    <span class="text-danger">
                                        <?= form_error('password') ?>
                                    </span>
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="role">Role</label>
                                    <select name="role" class="form-control" required id="role">
                                        <option selected disabled>-- Role --</option>
                                        <option value="admin" <?= ($admin['role'] == 'admin') ? 'selected' : '' ?>>Admin</option>
                                        <option value="kasir" <?= ($admin['role'] == 'kasir') ? 'selected' : '' ?>>Kasir</option>
                                    </select>
                                    <span class="text-danger">
                                        <?= form_error('role') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                            </div>
                        </fieldset>
                        <?= form_close() ?>
                    </div>
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>