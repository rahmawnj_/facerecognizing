<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                            <a href="<?= base_url('/dashboard/admin_create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                        </div>
                    </div>

                  
                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>


                    <div class="panel-body" class="table-responsive">
                        <table style="width: 100%;" id="data-table-default" class="table table-striped table-bordered align-middle">
                            <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;
                                foreach ($admins as $admin) : ?>
                                <tr>
                                    <td><?= ++$no; ?></td>
                                    <td>
                                        <img src="<?= base_url('/assets/img/uploads/' . $admin['photo']) ?>" class="img-fluid img-thumbnail mx-auto d-block text-center overflow-hidden" style="height:100px; width:100px" alt="">
                                    </td>
                                    <td><?= $admin['name'] ?></td>
                                    <td><?= $admin['username'] ?></td>
                                    <td><?= $admin['role'] ?></td>
                                   
                                        <td>
                                            <a class="fa btn-sm fa-edit btn bg-warning text-white" href="<?= base_url('dashboard/admin_edit/' . $admin['id_admin_face']) ?>"></a>
                                            <a id="delete-button" class="fas btn-sm fa-trash btn bg-danger text-white" href="<?= base_url('dashboard/admin_delete/' . $admin['id_admin_face']) ?>"></a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                        </table>
                    </div>

                    <!-- END hljs-wrapper -->
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>