<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>


    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">

                        </div>
                    </div>

                    <?= $this->session->flashdata('message'); ?>

                    <div class="panel-body">
                        <?php echo form_open_multipart('dashboard/admin_store'); ?>
                        <div class="img-preview d-flex">
                            <img src="<?= base_url('assets/img/default_user.jfif') ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:50px" alt="">
                        </div>
                        <?= $this->session->flashdata('message'); ?>
                        <fieldset>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="photo">Photo</label>
                                    <input required type="file" id="gambar" class="form-control" size="20" name="photo" id="photo" placeholder="Masukan photo">
                                    <span class="text-danger">
                                        <?= form_error('photo') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input autocomplete="off" type="text" class="form-control" value="<?= set_value('name') ?>" name="name" id="name" aria-describedby="name" placeholder="Masukan Nama" required>
                                    <span class="text-danger">
                                        <?= form_error('name') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input autocomplete="off" type="text" class="form-control" value="<?= set_value('username') ?>" name="username" id="username" aria-describedby="username" placeholder="Masukan Username" required>
                                    <span class="text-danger">
                                        <?= form_error('username') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="email">email</label>
                                    <input autocomplete="off" type="text" class="form-control" value="<?= set_value('email') ?>" name="email" id="email" aria-describedby="email" placeholder="Masukan email" required>
                                    <span class="text-danger">
                                        <?= form_error('email') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input autocomplete="off" type="password" class="form-control" required name="password" id="password" aria-describedby="password" placeholder="Masukan Password">
                                    <span class="text-danger">
                                        <?= form_error('password') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="role">Role</label>
                                    <select name="role" class="form-control" required id="role">
                                        <option selected disabled>-- Role --</option>
                                        <option value="admin">Admin</option>
                                        <option value="kasir">Kasir</option>
                                    </select>
                                    <span class="text-danger">
                                        <?= form_error('role') ?>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                            </div>
                        </fieldset>
                        <?= form_close() ?>
                    </div>
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>