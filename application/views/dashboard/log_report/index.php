<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                        </div>
                    </div>
                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

                    <div class="panel-body" class="table-responsive">
                        <form action="<?= base_url('dashboard/log_report') ?>" method="get" class="input-group m-3 input-group-sm">
                            <div class="input-group w-50 d-flex justify-content center">
                                <input type="date" value="<?= $this->input->get('startDate') ?>" name="startDate" class="form-control">
                                <input type="date" value="<?= $this->input->get('endDate') ?>" name="endDate" class="form-control">
                                <select name="member_type" id="" class="form-control">
                                    <option value="">-- All --</option>
                                    <option value="member" <?= ($this->input->get('member_type') == 'member') ? 'selected' : '' ?>>Member</option>
                                    <option value="visit" <?= ($this->input->get('member_type') == 'visit') ? 'selected' : '' ?>>Visit</option>
                                </select>
                                <button class="btn btn-secondary" type="submit"> Filter </button>
                            </div>
                        </form>

                        <table style="width: 100%;" id="data-table-report" class="table table-striped table-bordered align-middle">
                            <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th>Member ID</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Gate</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;
                                foreach ($log_reports as $log_report) : ?>
                                    <tr>
                                        <td><?= ++$no; ?></td>
                                        <td><?= $log_report['memberid'] ?></td>
                                        <td><?= $log_report['nama'] ?></td>
                                        <td><?= $log_report['alamat'] ?></td>
                                        <td><?= $log_report['device'] . ' - ' . $log_report['type'] ?></td>
                                        <td><?= $log_report['time'] ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- END hljs-wrapper -->
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layouts/dashboard/foot') ?>