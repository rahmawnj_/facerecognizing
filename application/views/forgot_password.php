<?php $this->load->view('layouts/dashboard/head') ?>
<main class="d-flex w-100 h-100">
    <div class="container d-flex flex-column">
        <div class="row vh-100">
            <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                <div class="d-table-cell align-middle">
                    <div class="card">
                        <div class="card-head">
                            <div class="text-center mt-4">
                                <h1 class="h2"><i class="align-middle me-2 fas fa-fw fa-universal-access"></i>TFITNESS</h1>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="m-sm-4">
                                <form action="<?= base_url('email/send_password_request') ?>" method="post" class="form-inline">
                                    <div class="form-group mr-2">
                                        <input type="email" name="email" class="form-control" placeholder="Email">
                                    </div>
                                    <button type="submit" class="btn btn-primary">Kirim</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $this->load->view('layouts/dashboard/foot') ?>