<!DOCTYPE html>
<html>

<head>
	<title>Log Image</title>
	<link rel="icon" href="<?= base_url('assets/img/logo.png') ?>">
	<link href="<?= base_url('assets/landingpage/bootstrap-5.3.0-alpha1-dist/css/bootstrap.min.css') ?>" rel="stylesheet" />

	<style>
		.card {
			flex-direction: row;
			align-items: center;
		}

		#message {
			text-align: center;
			/* rata tengah*/
			line-height: 400px;
			/* sesuaikan dengan tinggi card */
		}

		.card-title {
			font-weight: bold;
		}

		.card img {
			width: 30%;
			border-top-right-radius: 0;
			border-bottom-left-radius: calc(0.25rem - 1px);
		}

		@media only screen and (max-width: 768px) {
			a {
				display: none;
			}

			.card-body {
				padding: 0.5em 1.2em;
			}

			.card-body .card-text {
				margin: 0;
			}

			.card img {
				width: 50%;
			}
		}

		@media only screen and (max-width: 1200px) {
			.card img {
				width: 40%;
			}
		}

		.indicator {
			position: absolute;
			top: 0px;
			right: 0px;
			display: flex;
			align-items: center;
		}

		.status-circle {
			width: 20px;
			height: 20px;
			border-radius: 10px;
			margin-right: 5px;
		}

		.status {
			font-size: 18px;
			font-weight: bold;
		}
	</style>
</head>

<body style="background-color:midnightblue; margin:80px; overflow: hidden;">
	<div class="container">
		<div class="card" style="width: 100%; height: 80vh;">
			<div class="indicator" style="margin: 5px;" class="m-3">
				<div id="status-circle" class="status-circle"></div>
				<span id="status" class="status"></span>
			</div>
			<img id="image" style="display: none; height:100%;" class="card-img-top" alt="...">
			<div class="card-body">
				<h1 id="message" class="text-center">TFITNESS</h1>

				<h1 id="res-title" class="card-text"></h1>
				<h2 id="response-text" class="card-title"></h2>
				<h2 id="res-ket" class="card-text"></h2>
			</div>
		</div>
		<audio id="expireAudioPlayer" controls style="display: none">
			<source src="<?= base_url('/assets/music/uploads/setting/' . $this->db->get_where('setting', ['key' => 'expire_sound'])->row_array()['value']) ?>" type="audio/mpeg">
			Your browser does not support the audio element.
		</audio>
		<audio id="activeAudioPlayer" controls style="display: none">
			<source src="<?= base_url('/assets/music/uploads/setting/' . $this->db->get_where('setting', ['key' => 'active_sound'])->row_array()['value']) ?>" type="audio/mpeg">
			Your browser does not support the audio element.
		</audio>
	</div>

	<script src="<?= base_url('assets/landingpage/bootstrap-5.3.0-alpha1-dist/js/bootstrap.bundle.min.js') ?>" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
	<script>
		let soundPlayed = false; // Variable to track if the sound has been played
		let playedSounds = {
			expire: false,
			active: false
		};


		function stopExpireAudio() {
			var expireAudioPlayer = document.getElementById('expireAudioPlayer');
			expireAudioPlayer.pause();
			expireAudioPlayer.muted = true;
		}

		function playExpireAudio() {
			if (!playedSounds.expire) {
				var expireAudioPlayer = document.getElementById('expireAudioPlayer');
				expireAudioPlayer.play();
				expireAudioPlayer.muted = false;
				playedSounds.expire = true;
			}
		}

		function playActiveAudio() {
			if (!playedSounds.active) {
				var activeAudioPlayer = document.getElementById('activeAudioPlayer');
				activeAudioPlayer.play();
				activeAudioPlayer.muted = false;
				playedSounds.active = true;
			}
		}

		function stopActiveAudio() {
			var activeAudioPlayer = document.getElementById('activeAudioPlayer');
			activeAudioPlayer.pause();
			activeAudioPlayer.muted = true;
		}

		let lastImageUrl = '';
		let intervalId;
		let timeoutId;
		let isImageShown = false;
		let time = 0;
		let count = 0;

		document.addEventListener("DOMContentLoaded", function() {
			intervalId = setInterval(fetchData, 2000);
		});

		function fetchData() {
			let nowTime = Math.floor(Date.now() / 1000)
			count++;

			console.log(count);
			if (count % 1000 === 0) {
				location.reload();
			}
			fetch('<?= base_url('api/last_log/' . $id) ?>')
				.then(response => response.json())
				.then(data => {
					console.log(data)
					if (nowTime - data.logs.time_heartbeat > 10) {
						document.getElementById("status").innerHTML = "Offline";
						document.getElementById("status-circle").style.backgroundColor = 'red';
					} else {
						document.getElementById("status").innerHTML = "Online";
						document.getElementById("status-circle").style.backgroundColor = 'green';
					}

					if (nowTime - data.logs.time < 20) {
						if (data.logs.image) {
							// Simpan URL gambar yang didapat dari API
							lastImageUrl = data.logs.image;
							// Tampilkan gambar yang didapat dari API
							document.getElementById("message").innerHTML = '';
							document.getElementById("image").src = data.logs.image;
							document.getElementById("image").style.display = 'block';
							document.getElementById("image").style.height = '80%';
							document.getElementById("image").style.width = '25%';
							document.getElementById("image").style.marginLeft = '10%';
							document.getElementById("image").style.marginBottom = '5%';
							isImageShown = true;
							// Tampilkan response text
							if (data.logs.operator === 'VerifyPush') {
								let checkExpire = '';
								document.getElementById("res-title").style.marginBottom = '10px';
								document.getElementById("res-title").style.textAlign = 'center';
								document.getElementById("response-text").innerHTML = 'NAMA : ' + data.data_user.nama;
								document.getElementById("response-text").style.textAlign = 'center';
								if (data.data_user.jatuhtempo) {
									document.getElementById("res-ket").innerHTML = 'MASA AKTIV : ' + data.data_user.jatuhtempo;
									document.getElementById("res-ket").style.textAlign = 'center';
								}

								if (new Date() >= new Date(data.data_user.jatuhtempo)) {
									playExpireAudio();
									stopActiveAudio();
									checkExpire = 'Status Member : Kadaluarsa';
									if (playedSounds.active) {
										stopActiveAudio();
									}
								} else {
									// if (data.data_user.keterangan && data.data_user.keterangan.includes("Personal Trainer")) {
									// 	checkExpire = 'Status Member : Kadaluarsa';
									// 	playExpireAudio();
									// 	stopActiveAudio();
									// } else {
									// playActiveAudio();
									stopExpireAudio();
									checkExpire = 'Status Member : Aktif';
									if (!playedSounds.active) {
										playActiveAudio();
									}
									// Hentikan pemutaran musik jika status Kadaluarsa.
									// }
								}
							} else {
								// document.getElementById("res-title").innerHTML = 'TFITNESS';
								document.getElementById("response-text").innerHTML = 'TIDAK TERDAFTAR';
								document.getElementById("res-ket").innerHTML = '';
								document.getElementById("res-ket").style.textAlign = 'center';
								document.getElementById("response-text").style.textAlign = 'center';
								document.getElementById("res-title").style.textAlign = 'center';
								stopActiveAudio();
								stopExpireAudio(); // Hentikan pemutaran musik jika status Kadaluarsa.
							}
							clearTimeout(timeoutId);
							timeoutId = setTimeout(hideImage, 2000);
							time = 0;
							console.log(timeoutId);
						}
					}

					if (playedSounds.expire) {
						stopExpireAudio();
					}
				});
		}

		function hideImage() {
			stopActiveAudio();
			stopExpireAudio();
			soundPlayed = false;
			document.getElementById("image").style.display = 'none';
			isImageShown = false;
			document.getElementById("message").innerHTML = 'TFITNESS';
			document.getElementById("res-ket").innerHTML = '';
			document.getElementById("response-text").innerHTML = '';
			// document.getElementById("res-berlaku").innerHTML = '';
			// document.getElementById("res-jatuhtempo").innerHTML = '';
			document.getElementById("res-title").innerHTML = '';


		}
	</script>
</body>

</html>